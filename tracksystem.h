#pragma once

#include "trainstation.h"
#include "railroad.h"
#include "tracktilebase.h"


#include "colorshader.h"
//#include <d3d11.h>
#include "d3dclass.h"
#include "light.h"

#include <vector>

using namespace std;

class TrackSystem
{

	public:
		TrackSystem(void);
		~TrackSystem(void);

		bool Initialize(ID3D11Device*);
		void Shutdown();

		void AddRailroadTrack(RailRoad*);
		void RemoveRailroadTrack(RailRoad*);

		void AddTrainStation(TrainStation*);
		void RemoveTrainStation(TrainStation*);

		vector<TrainStation*> getTrainStationCollection();
		vector<RailRoad*> getRailroadTrackCollection();

		void Render(ColorShaderClass* colorShader, D3DClass* direct3D, LightClass* light, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix);
		
	private:

		vector<TrainStation*> m_TrainStationObjects;
		vector<RailRoad*> m_RailroadTrackObjects;
		//vector<stationtest*> m_TrainStationObjects;
		//vector<railroadtest*> m_RailroadTrackObjects;

		//TODO placeholders
		bool InitTestStations(ID3D11Device* device);
		bool InitTestTracks(ID3D11Device* device);

		RailRoad* testTrack;
		TrainStation* testStation;
		TrainStation* testStation2;

		//TrainStation* m_trainStation;
		//TrainStation* m_trainStation2;
		//RailRoad* m_TestTrackNode;


};

