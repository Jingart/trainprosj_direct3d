#include "traincartclass.h"


TrainCartClass::TrainCartClass(void)
	: TrainObject()
{

	m_positionX = 0.0f;
	m_positionY = 1.0f;
	m_positionZ = 0.0f;

	m_scaleX = 0.15f;
	m_scaleY = 0.1f;
	m_scaleZ = 0.4f;

	m_canMove = false;
	m_cargoCapacity = 1;
	m_trainObjectType = CART_BOX;
}


TrainCartClass::~TrainCartClass(void)
{
}


void TrainCartClass::Shutdown()
{

	for (int i = 0; i < m_cargoList.size(); i++)
		m_cargoList[i]->Shutdown();

}


bool TrainCartClass::AddCargo(CargoBox* cargo)
{
	bool result = true;
	if (m_cargoList.size() <= m_cargoCapacity)
	{
		//cargo->SetPosition( m_positionX + (1.0f * cargo->GetScaleX()), m_positionY,  m_positionZ  + (1.0f * cargo->GetScaleZ()));
		cargo->SetPosition( m_positionX, m_positionY + 2.0f, m_positionZ );
		m_cargoList.push_back(cargo);
	}
	else
		result = false;

	return result;
}



void TrainCartClass::PushToNextTile()
{
	TrainObject::PushToNextTile();
	//TODO dyn height
	for(int i = 0; i < m_cargoList.size(); i++)
		m_cargoList[i]->SetPosition( m_positionX,
								     m_positionY + 2.0f,  
									 m_positionZ );
}

void TrainCartClass::SetTrack(RailRoad* trackObject)
{
	TrainObject::SetTrack(trackObject);
	//TODO dyn height
	for(int i = 0; i < m_cargoList.size(); i++)
		m_cargoList[i]->SetPosition( m_positionX, //+ (1.0f * m_cargoList[i]->GetScaleX()), 
								     m_positionY + 2.0f,  
									 m_positionZ );//  + (1.0f * m_cargoList[i]->GetScaleZ()));
}


vector<CargoBox*> TrainCartClass::GetCargoVector()
{
	return m_cargoList;
}


void TrainCartClass::MoveToDestination()
{
	TrainObject::MoveToDestination();

	for (int i = 0; i < m_cargoList.size(); i++)
	{
		//TODO dynamic height
		m_cargoList[i]->SetPosition(m_positionX, m_positionY + 2.0f, m_positionZ);
		m_cargoList[i]->SetRotation(m_rotationX, m_rotationY, m_rotationZ);
	}
}