#pragma once

//TODO
#include "railroadtrack.h"
#include "modelobject.h"

using namespace std;

class Plant : public ModelObject
{
public:
	struct TileCoordinat
	{
		int x;
	    int y;
	};

	enum  PlantTypeEnum{ BOXPLANT, COALPLANT};

	Plant(void);
	~Plant(void);
	void StartProduction();
	void StopProduction();

	void SetFrameTime(float);

protected: 
	bool m_isProducing;
	PlantTypeEnum m_plantType;

	float m_frameTime;
	float m_time;

};

