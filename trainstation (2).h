#pragma once
#include "tracktilebase.h"

class TrainStation2 : public TrackTileBase
{
public:
	TrainStation2(void);
	TrainStation2(TrackTile startTile, TrackTile endTile);
	~TrainStation2(void);

	bool Initialize(ID3D11Device* device);

private:
	TrackTile m_startTile;
	TrackTile m_endTile;
	D3DXVECTOR4 m_color;
	int m_stationLength;
};

