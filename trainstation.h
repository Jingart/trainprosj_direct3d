#pragma once
#include "tracktilebase.h"

class TrainStation : public TrackTileBase
{
public:
	TrainStation(void);
	TrainStation(TrackTile, TrackTile);
	~TrainStation(void);

	bool Initialize(ID3D11Device* device);

private:
	TrackTile m_startTile;
	TrackTile m_endTile;
	//D3DXVECTOR4 m_color;
	int m_stationLength;
};

