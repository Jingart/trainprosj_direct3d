#include "plant.h"


Plant::Plant(void)
	: ModelObject()
{
	m_time = 0;
	m_frameTime = 0;
	m_isProducing = false;
}


Plant::~Plant(void)
{
}


void Plant::StartProduction()
{
	m_isProducing = true;
}


void Plant::StopProduction()
{
	m_isProducing = false;
}


void Plant::SetFrameTime(float frameTime)
{
	m_frameTime = frameTime;
	m_time += frameTime;
}