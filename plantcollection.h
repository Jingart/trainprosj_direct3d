#pragma once
#include "plant.h"
#include "plantbox.h"
#include <vector>

#include "colorshader.h"
#include "d3dclass.h"
#include "light.h"

class PlantCollection
{
public:
	PlantCollection(void);
	~PlantCollection(void);

	void Shutdown();

	void AddPlant(Plant*);
	void AddPlant(PlantBox*);
	void RemovePlant(int);

	void SetFrameTime(float);

	void Render(ColorShaderClass* colorShader, D3DClass* direct3D, LightClass* light, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix);

private:
	vector<Plant*> m_plantCollection;
	vector<PlantBox*> m_plantBoxCollection;
};

