#include "trainobject.h"


TrainObject::TrainObject(void)
	: ModelObject()
{

	m_positionX = 0.0f;
	m_positionY = 1.0f;
	m_positionZ = 0.0f;

	m_scaleX = 0.15f;
	m_scaleY = 0.15f;
	m_scaleZ = 0.4f;

	m_canMove = false;
	//m_trackPosition = -1;
	//trackPositionIterator
	//m_destination = -1;
}
//
//
//trainobject::~trainobject(void)
//{
//}

//void TrainObject::SetTrackPosition(int trackPosition)
//{
//	m_trackPosition = trackPosition;
//
//	//list<RailRoad::TileCoordinat>::iterator trackIterator;
//
//	m_positionX = m_track[m_trackPosition].x + 0.5f;
//
//	m_positionZ = m_track[m_trackPosition].y + 0.5f;
//
//
//}


void TrainObject::SetTrack(RailRoad* trackObject)
{
	m_trackObject = trackObject;
	m_trackObject->GetTrack(m_track);

	m_currentPosition = m_track.begin();
	UpdatePositionOnTrack();
}


void TrainObject::PushToNextTile()
{
	m_currentPosition++;
	UpdatePositionOnTrack();
}


void TrainObject::PushToPreviousTile()
{
	m_currentPosition--;
	UpdatePositionOnTrack();
}


void TrainObject::UpdatePositionOnTrack()
{
	m_positionX = m_currentPosition->x + 0.5f;
	m_positionZ = m_currentPosition->y + 0.5f;
}


list<RailRoad::TrackTile>::iterator* TrainObject::GetCurrentPosition()
{
	return &m_currentPosition;
}


void TrainObject::SetCurrentPosition(list<RailRoad::TrackTile>::iterator position)
{
	m_currentPosition = position;
}


//void TrainObject::MoveForward()
//{
//	m_forwardSpeed += m_frameTime * 0.0000001f;
//	float radians = m_rotationY * 0.0174532925f;
//	m_positionX += sinf(radians) * m_forwardSpeed;
//	m_positionZ += cosf(radians) * m_forwardSpeed;
//	//m_positionX += m_forwardSpeed;
//	return;
//}


void TrainObject::MoveToDestination()
{
	if (m_currentPosition != m_track.end())
	{


		//int currentTile = m_trackPosition;
		//int nextTile = m_trackPosition + 1;
		//int nextNextTile = m_trackPosition + 2;
		list<RailRoad::TrackTile>::iterator nextPosition;
		nextPosition = m_currentPosition;
		nextPosition++;

		list<RailRoad::TrackTile>::iterator nextNextPosition;
		nextNextPosition = nextPosition;
		nextNextPosition++;

		float currentTileX = m_currentPosition->x + 0.5f;
		float currentTileY = m_currentPosition->y + 0.5f;
		float nextTileX = nextPosition->x + 0.5f;
		float nextTileY = nextPosition->y + 0.5f;

		

		if(m_rotationY == 0)
		{
			// if neste tile g�r skr� opp h�gre
			if (currentTileX + 1 == nextTileX && currentTileY + 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 45;
			} // // if neste tile g�r skr� opp venstre
			else if (currentTileX - 1 == nextTileX && currentTileY + 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 315;
			} // if ein tile rett fram
			else if (false)
			{
				m_forwardSpeed = 0;
				m_canMove = false;
			}
		}

		else if(m_rotationY == 45)
		{
			// if neste tile g�r h�gre
			if (currentTileX + 1 == nextTileX && currentTileY == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 90;
			} // // if neste tile g�r opp
			else if (currentTileX == nextTileX && currentTileY + 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 0;
			} // if ein tile rett fram
			else if (false)
			{
				m_forwardSpeed = 0;
				m_canMove = false;
			}
		}

		else if(m_rotationY == 90)
		{
			// if neste tile g�r h�gre ned 
			if (currentTileX + 1 == nextTileX && currentTileY - 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 135;
			} // // if neste tile h�gre opp
			else if (currentTileX + 1 == nextTileX && currentTileY + 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 45;
			} // if ein tile rett fram
			else if (false)
			{
				m_forwardSpeed = 0;
				m_canMove = false;
			}
		}

		else if(m_rotationY == 135)
		{
			// if neste tile g�r ned 
			if (currentTileX == nextTileX && currentTileY - 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 180;
			} // // if neste tile h�gre
			else if (currentTileX + 1 == nextTileX && currentTileY == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 90;
			} // if ein tile rett fram
			else if (false)
			{
				m_forwardSpeed = 0;
				m_canMove = false;
			}
		}

		else if(m_rotationY == 180)
		{
			// if neste tile g�r h�gre ned 
			if (currentTileX + 1 == nextTileX && currentTileY - 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 135;
			} // // if neste tile venstre ned
			else if (currentTileX - 1 == nextTileX && currentTileY - 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 225;
			} // if ein tile rett fram
			else if (false)
			{
				m_forwardSpeed = 0;
				m_canMove = false;
			}
		}

		else if(m_rotationY == 225)
		{
			// if neste tile g�r venstre
			if (currentTileX - 1 == nextTileX && currentTileY == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 270;
			} // // if neste tile ned
			else if (currentTileX  == nextTileX && currentTileY - 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 180;
			} // if ein tile rett fram
			else if (false)
			{
				m_forwardSpeed = 0;
				m_canMove = false;
			}
		}

		else if(m_rotationY == 270)
		{
			// if neste tile g�r venstre ned
			if (currentTileX - 1 == nextTileX && currentTileY - 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 225;
			} // // if neste tile g�r venstre opp
			else if (currentTileX - 1  == nextTileX && currentTileY + 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 315;
			} // if ein tile rett fram
			else if (false)
			{
				m_forwardSpeed = 0;
				m_canMove = false;
			}
		}

		else if(m_rotationY == 315)
		{
			// if neste tile g�r opp
			if (currentTileX == nextTileX && currentTileY + 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 0;
			} // // if neste tile g�r venstre
			else if (currentTileX - 1  == nextTileX && currentTileY == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 270;
			} // if ein tile rett fram
			else if (false)
			{
				m_forwardSpeed = 0;
				m_canMove = false;
			}
		}

		//if (m_forwardSpeed <  0.0007f) 
		//	m_forwardSpeed += m_frameTime * 0.000001f;

		if (m_forwardSpeed <  0.09f) 
			m_forwardSpeed += m_frameTime * 0.000005f;

		float radians = m_rotationY * 0.0174532925f;
		m_positionX += sinf(radians) * m_forwardSpeed;
		m_positionZ += cosf(radians) * m_forwardSpeed;


		if (m_positionZ < (float)nextTileY + 0.05f && m_positionZ > (float)nextTileY - 0.05f && m_positionX < (float)nextTileX + 0.05f && m_positionX > (float)nextTileX - 0.05f)
			m_currentPosition++;

	}else
	{
		m_forwardSpeed = 0;
		m_canMove = false;
	}


	return;


	// viss ikkje framme eller dest ikkje er n�dd
	//if (m_destination != -1 && m_trackPosition < m_destination)
	//{


	//	int currentTile = m_trackPosition;
	//	int nextTile = m_trackPosition + 1;
	//	int nextNextTile = m_trackPosition + 2;

	//	float currentTileX = m_track[currentTile].x + 0.5f;
	//	float currentTileY = m_track[currentTile].y + 0.5f;
	//	float nextTileX = m_track[nextTile].x + 0.5f;
	//	float nextTileY = m_track[nextTile].y + 0.5f;

	//	

	//	if(m_rotationY == 0)
	//	{
	//		// if neste tile g�r skr� opp h�gre
	//		if (currentTileX + 1 == nextTileX && currentTileY + 1 == nextTileY)
	//		{
	//				m_positionX = (float)currentTileX;
	//				m_positionZ = (float)currentTileY;
	//				m_rotationY = 45;
	//		} // // if neste tile g�r skr� opp venstre
	//		else if (currentTileX - 1 == nextTileX && currentTileY + 1 == nextTileY)
	//		{
	//				m_positionX = (float)currentTileX;
	//				m_positionZ = (float)currentTileY;
	//				m_rotationY = 315;
	//		} // if ein tile rett fram
	//		else if (false)
	//		{
	//			m_forwardSpeed = 0;
	//			m_canMove = false;
	//		}
	//	}

	//	else if(m_rotationY == 45)
	//	{
	//		// if neste tile g�r h�gre
	//		if (currentTileX + 1 == nextTileX && currentTileY == nextTileY)
	//		{
	//				m_positionX = (float)currentTileX;
	//				m_positionZ = (float)currentTileY;
	//				m_rotationY = 90;
	//		} // // if neste tile g�r opp
	//		else if (currentTileX == nextTileX && currentTileY + 1 == nextTileY)
	//		{
	//				m_positionX = (float)currentTileX;
	//				m_positionZ = (float)currentTileY;
	//				m_rotationY = 0;
	//		} // if ein tile rett fram
	//		else if (false)
	//		{
	//			m_forwardSpeed = 0;
	//			m_canMove = false;
	//		}
	//	}

	//	else if(m_rotationY == 90)
	//	{
	//		// if neste tile g�r h�gre ned 
	//		if (currentTileX + 1 == nextTileX && currentTileY - 1 == nextTileY)
	//		{
	//				m_positionX = (float)currentTileX;
	//				m_positionZ = (float)currentTileY;
	//				m_rotationY = 135;
	//		} // // if neste tile h�gre opp
	//		else if (currentTileX + 1 == nextTileX && currentTileY + 1 == nextTileY)
	//		{
	//				m_positionX = (float)currentTileX;
	//				m_positionZ = (float)currentTileY;
	//				m_rotationY = 45;
	//		} // if ein tile rett fram
	//		else if (false)
	//		{
	//			m_forwardSpeed = 0;
	//			m_canMove = false;
	//		}
	//	}

	//	else if(m_rotationY == 135)
	//	{
	//		// if neste tile g�r ned 
	//		if (currentTileX == nextTileX && currentTileY - 1 == nextTileY)
	//		{
	//				m_positionX = (float)currentTileX;
	//				m_positionZ = (float)currentTileY;
	//				m_rotationY = 180;
	//		} // // if neste tile h�gre
	//		else if (currentTileX + 1 == nextTileX && currentTileY == nextTileY)
	//		{
	//				m_positionX = (float)currentTileX;
	//				m_positionZ = (float)currentTileY;
	//				m_rotationY = 90;
	//		} // if ein tile rett fram
	//		else if (false)
	//		{
	//			m_forwardSpeed = 0;
	//			m_canMove = false;
	//		}
	//	}

	//	else if(m_rotationY == 180)
	//	{
	//		// if neste tile g�r h�gre ned 
	//		if (currentTileX + 1 == nextTileX && currentTileY - 1 == nextTileY)
	//		{
	//				m_positionX = (float)currentTileX;
	//				m_positionZ = (float)currentTileY;
	//				m_rotationY = 135;
	//		} // // if neste tile venstre ned
	//		else if (currentTileX - 1 == nextTileX && currentTileY - 1 == nextTileY)
	//		{
	//				m_positionX = (float)currentTileX;
	//				m_positionZ = (float)currentTileY;
	//				m_rotationY = 225;
	//		} // if ein tile rett fram
	//		else if (false)
	//		{
	//			m_forwardSpeed = 0;
	//			m_canMove = false;
	//		}
	//	}

	//	else if(m_rotationY == 225)
	//	{
	//		// if neste tile g�r venstre
	//		if (currentTileX - 1 == nextTileX && currentTileY == nextTileY)
	//		{
	//				m_positionX = (float)currentTileX;
	//				m_positionZ = (float)currentTileY;
	//				m_rotationY = 270;
	//		} // // if neste tile ned
	//		else if (currentTileX  == nextTileX && currentTileY - 1 == nextTileY)
	//		{
	//				m_positionX = (float)currentTileX;
	//				m_positionZ = (float)currentTileY;
	//				m_rotationY = 180;
	//		} // if ein tile rett fram
	//		else if (false)
	//		{
	//			m_forwardSpeed = 0;
	//			m_canMove = false;
	//		}
	//	}

	//	else if(m_rotationY == 270)
	//	{
	//		// if neste tile g�r venstre ned
	//		if (currentTileX - 1 == nextTileX && currentTileY - 1 == nextTileY)
	//		{
	//				m_positionX = (float)currentTileX;
	//				m_positionZ = (float)currentTileY;
	//				m_rotationY = 225;
	//		} // // if neste tile g�r venstre opp
	//		else if (currentTileX - 1  == nextTileX && currentTileY + 1 == nextTileY)
	//		{
	//				m_positionX = (float)currentTileX;
	//				m_positionZ = (float)currentTileY;
	//				m_rotationY = 315;
	//		} // if ein tile rett fram
	//		else if (false)
	//		{
	//			m_forwardSpeed = 0;
	//			m_canMove = false;
	//		}
	//	}

	//	else if(m_rotationY == 315)
	//	{
	//		// if neste tile g�r opp
	//		if (currentTileX == nextTileX && currentTileY + 1 == nextTileY)
	//		{
	//				m_positionX = (float)currentTileX;
	//				m_positionZ = (float)currentTileY;
	//				m_rotationY = 0;
	//		} // // if neste tile g�r venstre
	//		else if (currentTileX - 1  == nextTileX && currentTileY == nextTileY)
	//		{
	//				m_positionX = (float)currentTileX;
	//				m_positionZ = (float)currentTileY;
	//				m_rotationY = 270;
	//		} // if ein tile rett fram
	//		else if (false)
	//		{
	//			m_forwardSpeed = 0;
	//			m_canMove = false;
	//		}
	//	}

	//	//if (m_forwardSpeed <  0.0007f) 
	//	//	m_forwardSpeed += m_frameTime * 0.000001f;

	//	if (m_forwardSpeed <  0.09f) 
	//		m_forwardSpeed += m_frameTime * 0.000005f;

	//	float radians = m_rotationY * 0.0174532925f;
	//	m_positionX += sinf(radians) * m_forwardSpeed;
	//	m_positionZ += cosf(radians) * m_forwardSpeed;


	//	if (m_positionZ < (float)nextTileY + 0.05f && m_positionZ > (float)nextTileY - 0.05f && m_positionX < (float)nextTileX + 0.05f && m_positionX > (float)nextTileX - 0.05f)
	//		m_trackPosition++;

	//}else
	//{
	//	m_forwardSpeed = 0;
	//	m_canMove = false;
	//}


	//return;
}

//
//int TrainObject::GetTrackPosition()
//{
//	return m_trackPosition;
//}

