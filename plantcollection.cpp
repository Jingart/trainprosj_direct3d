#include "plantcollection.h"


PlantCollection::PlantCollection(void)
{
}


PlantCollection::~PlantCollection(void)
{
}


void PlantCollection::Shutdown()
{;
	for(int i = 0; i < m_plantCollection.size(); i++)
		m_plantCollection[i]->Shutdown();

	for(int i = 0; i < m_plantBoxCollection.size(); i++)
		m_plantBoxCollection[i]->Shutdown();
}


void PlantCollection::SetFrameTime(float time)
{

	for(int i = 0; i < m_plantCollection.size(); i++)
		m_plantCollection[i]->SetFrameTime(time);

	for(int i = 0; i < m_plantBoxCollection.size(); i++)
		m_plantBoxCollection[i]->SetFrameTime(time);

	return;
}


void PlantCollection::AddPlant(PlantBox* plant)
{
	m_plantBoxCollection.push_back(plant);
}


void PlantCollection::Render(ColorShaderClass* colorShader, D3DClass* direct3D, LightClass* light, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix)
{
	bool result;

	D3DXMATRIX modelWorldMatrix;

	for(int i = 0; i < m_plantBoxCollection.size(); i++)
	{

		vector<CargoBox*> *m_cargoList = m_plantBoxCollection[i]->GetCargoShippingList();

		for (int i = 0; i < m_cargoList->size(); i++)
		{
			CargoBox *cargoBox = m_cargoList->at(i);
			cargoBox->GetMatrix(modelWorldMatrix);
			cargoBox->Render(direct3D->GetDeviceContext());
			result = colorShader->Render(direct3D->GetDeviceContext(), cargoBox->GetIndexCount(), modelWorldMatrix, viewMatrix, projectionMatrix, 
							light->GetDirection(), light->GetAmbientColor(), light->GetDiffuseColor());
		}

		m_plantBoxCollection[i]->GetMatrix(modelWorldMatrix);
		m_plantBoxCollection[i]->Render(direct3D->GetDeviceContext());
		result = colorShader->Render(direct3D->GetDeviceContext(), m_plantBoxCollection[i]->GetIndexCount(), modelWorldMatrix, viewMatrix, projectionMatrix, 
						   light->GetDirection(), light->GetAmbientColor(), light->GetDiffuseColor());
	}

}