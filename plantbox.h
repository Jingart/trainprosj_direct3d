#pragma once
#include "plant.h"
#include "cargobox.h"

class PlantBox : public Plant
{
public:



	PlantBox(void);
	PlantBox(float x, float y, float z);
	PlantBox(float x, float z, float rotation, D3DXVECTOR4 color);
	~PlantBox(void);
	bool Initialize(ID3D11Device* device);
	//void SetFrameTime(float);

	vector<CargoBox*>* GetCargoShippingList();

private:
	vector<CargoBox*> m_cargoShippingList;
	TileCoordinat cargoArea;
	
};

