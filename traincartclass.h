#pragma once
#include "trainobject.h"
#include "cargobox.h"
#include <vector>


class TrainCartClass : public TrainObject
{
public:
	
	TrainCartClass(void);
	~TrainCartClass(void);
	void Shutdown();

	void RenderCargo();

	vector<CargoBox*> GetCargoVector();
	bool AddCargo(CargoBox* cargo);
	void RemoveCargo();
	void MoveToDestination();
	void SetTrackPosition(int);
	void PushToNextTile();

	void SetTrack(RailRoad* trackObject);
	//void UpdatePositionOnTrack();

	//Set
private:
	vector<CargoBox*> m_cargoList;
	int m_cargoCapacity;
};

