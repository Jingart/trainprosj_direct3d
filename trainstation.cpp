#include "trainstation.h"


TrainStation::TrainStation(void)
{
}


TrainStation::TrainStation(TrackTile startTile, TrackTile endTile)
{

	m_vertexBuffer = 0;
	m_indexBuffer = 0;
	m_stationLength = 0;

	//TODO color is crazy
	m_color = D3DXVECTOR4(0.8f, 0.2f, 0.5f, 0.5f);
	m_startTile = startTile;
	m_endTile = endTile;

}


TrainStation::~TrainStation(void)
{
}


bool TrainStation::Initialize(ID3D11Device* device)
{
	bool result;

	TrackTile tmpCoord;
	tmpCoord.x = m_startTile.x;
	tmpCoord.y = m_startTile.y;

	trackList.push_back(m_startTile);

	//TODO ikkje sikkert stasjonen e bein
	int length = fabs((float)(m_endTile.x - m_startTile.x) + (m_endTile.y - m_startTile.y));
	for(int i = 0; i < length - 1; i++)
	{
		if ((m_startTile.x + i) < m_endTile.x)
			tmpCoord.x++;

		if ((m_startTile.y + i) < m_endTile.y)
			tmpCoord.y++;

		trackList.push_back(tmpCoord);
	}



	result = InitializeBuffers(device);
	if(!result)
	{
		return false;
	}

	return true;
}


