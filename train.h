#pragma once
#include "trainobject.h"
#include "locomotiveobject.h"
#include "traincartclass.h"


#include "colorshader.h"
#include "d3dclass.h"
#include "light.h"

#include <vector>

using namespace std;

class Train
{
public:
	Train(void);
	~Train(void);
	
	bool Initialize(ID3D11Device*);
	void Shutdown();
	void AddTrainObject();
	void SetFrameTime(float);

	void SetTrack(RailRoad*);
	void MoveToDestination();

	void SetTrainPosition(float x, float z, float rotation);

	//TODO
	void AddCargo();
	void RemoveCargo();
	void MoveForward();
	void MoveBackwards();
	void CheckForTrack();

	void Render(ColorShaderClass* colorShader, D3DClass* direct3D, LightClass* light, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix);

private:

	void AlignTrainOnTrack();
	void AlignLocomotive();
	void AlignTrainObjects();

	LocomotiveObject* m_locomotive;
	vector<TrainCartClass*> m_trainobjects;

	RailRoad* m_RailRoadTrack;

};

