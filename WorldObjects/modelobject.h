#pragma once
#include "modelclass.h"

class ModelObject
{
public:
	ModelObject(void);
	~ModelObject(void);

	bool Initialize(ID3D11Device*);
	void Shutdown();
	void Render(ID3D11DeviceContext*);

	void SetFrameTime(float);
	void SetPosition(float, float, float);
	void SetScale(float, float , float);
	void SetRotation(float, float , float);
	void SetColor(D3DXVECTOR4 color);

	float GetPostitionX();
	float GetPostitionY();
	float GetPostitionZ();
	float GetRotationX();
	float GetRotationY();
	float GetRotationZ();
	float GetSpeed();

	void GetMatrix(D3DXMATRIX& modelMatrix);
	int GetIndexCount();

	void MoveForward();


protected:
	ModelClass* m_Model;

	float m_positionX;
	float m_positionY;
	float m_positionZ;

	float m_scaleX;
	float m_scaleY;
	float m_scaleZ;

	float m_rotationX;
	float m_rotationY;
	float m_rotationZ;

	float m_frameTime;
	float m_forwardSpeed;

	D3DXVECTOR4 m_color;

	D3DXMATRIX m_scaleMatrix;
	D3DXMATRIX m_translateMatrix;
	D3DXMATRIX m_rotationMatrix;
};

