////////////////////////////////////////////////////////////////////////////////
// Filename: modelclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "modelclass.h"


ModelClass::ModelClass()
{
	m_vertexBuffer = 0;
	m_indexBuffer = 0;
	m_color = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
	//m_frameTime = 0.0f;
	//m_forwardSpeed = 0.0f;
	//m_positionX = 0.0f;
	//m_positionY = 0.0f;
	//m_positionZ = 0.0f;
	//m_trackPosition = 0;
}


ModelClass::ModelClass(const ModelClass& other)
{
}


//ModelClass::ModelClass(float x, float y, float z)
//{
//	m_positionX = x;
//	m_positionY = y;
//	m_positionZ = z;
//}

ModelClass::~ModelClass()
{
}


bool ModelClass::Initialize(ID3D11Device* device)
{
	bool result;


	// Initialize the vertex and index buffer that hold the geometry for the triangle.
	result = InitializeBuffers(device);
	if(!result)
	{
		return false;
	}

	return true;
}


bool ModelClass::Initialize(ID3D11Device* device, char* modelFilename)
{
	bool result;

	// Load in the model data,
	result = LoadModel(modelFilename);
	if(!result)
	{
		return false;
	}

	// Initialize the vertex and index buffer that hold the geometry for the triangle.
	result = InitializeBuffers2(device);
	if(!result)
	{
		return false;
	}

	return true;
}


void ModelClass::Shutdown()
{
	// Release the vertex and index buffers.
	ShutdownBuffers();

	return;
}


void ModelClass::Render(ID3D11DeviceContext* deviceContext)
{
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(deviceContext);

	return;
}


int ModelClass::GetIndexCount()
{
	return m_indexCount;
}


bool ModelClass::InitializeBuffers2(ID3D11Device* device)
{
	VertexType* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;

	// Set the number of vertices in the vertex array.
	//m_vertexCount = 3;

	// Set the number of indices in the index array.
	//m_indexCount = 3;

	// Create the vertex array.
	vertices = new VertexType[m_vertexCount];
	if(!vertices)
	{
		return false;
	}

	// Create the index array.
	indices = new unsigned long[m_indexCount];
	if(!indices)
	{
		return false;
	}



	// Load the vertex array and index array with data.
	for(int i=0; i<m_vertexCount; i++)
	{
		vertices[i].position = D3DXVECTOR3(m_model[i].x, m_model[i].y, m_model[i].z);
		vertices[i].color = m_color;//D3DXVECTOR4(0.14f, 0.34f, 0.60f, 1.0f);
		vertices[i].normal = D3DXVECTOR3(m_model[i].nx, m_model[i].ny, m_model[i].nz);

		indices[i] = i;
	}

	// Set up the description of the static vertex buffer.
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexType) * m_vertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_indexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete [] vertices;
	vertices = 0;

	delete [] indices;
	indices = 0;

	return true;
}


bool ModelClass::InitializeBuffers(ID3D11Device* device)
{
	VertexType* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;

	// Set the number of vertices in the vertex array.
	m_vertexCount = 8;

	// Set the number of indices in the index array.
	m_indexCount = 36;

	// Create the vertex array.
	vertices = new VertexType[m_vertexCount];
	if(!vertices)
	{
		return false;
	}

	// Create the index array.
	indices = new unsigned long[m_indexCount];
	if(!indices)
	{
		return false;
	}

	// Load the vertex array with data.
	vertices[0].position = D3DXVECTOR3(-1.0f, 1.0f, -1.0f);  // Bottom left.
	vertices[0].color = m_color;// D3DXVECTOR4(0.14f, 0.34f, 0.60f, 1.0f);
	vertices[0].normal = D3DXVECTOR3(1.0f, 0.0f, 1.0f);

	vertices[1].position = D3DXVECTOR3(1.0f, 1.0f, -1.0f);  // Top left.
	vertices[1].color = m_color;//D3DXVECTOR4(0.14f, 0.34f, 0.60f, 1.0f);
	vertices[1].normal = D3DXVECTOR3(1.0f, 1.0f, 0.0f);

	vertices[2].position = D3DXVECTOR3(1.0f, 1.0f, 1.0f);  // Top right.
	vertices[2].color = m_color;//D3DXVECTOR4(0.14f, 0.34f, 0.60f, 1.0f);
	vertices[2].normal = D3DXVECTOR3(1.0f, 1.0f, 0.0f);

	vertices[3].position = D3DXVECTOR3(-1.0f, 1.0f, 1.0f);  // Bottom right.
	vertices[3].color = m_color;//D3DXVECTOR4(0.14f, 0.34f, 0.60f, 1.0f);
	vertices[3].normal = D3DXVECTOR3(1.0f, 1.0f, 0.0f);


	vertices[4].position = D3DXVECTOR3(-1.0f, -1.0f, -1.0f);  // Bottom right.
	vertices[4].color = m_color;//D3DXVECTOR4(0.14f, 0.34f, 0.60f, 1.0f);
	vertices[4].normal = D3DXVECTOR3(0.0f, 1.0f, 0.0f);

	vertices[5].position = D3DXVECTOR3(1.0f, -1.0f, -1.0f);  // Bottom right.
	vertices[5].color = m_color;//D3DXVECTOR4(0.14f, 0.34f, 0.60f, 1.0f);
	vertices[5].normal = D3DXVECTOR3(0.0f, 1.0f, 0.0f);

	vertices[6].position = D3DXVECTOR3(1.0f, -1.0f, 1.0f);  // Bottom right.
	vertices[6].color = m_color;//D3DXVECTOR4(0.14f, 0.34f, 0.60f, 1.0f);
	vertices[6].normal = D3DXVECTOR3(0.0f, 1.0f, 0.0f);

	vertices[7].position = D3DXVECTOR3(-1.0f, -1.0f, 1.0f);  // Bottom right.
	vertices[7].color = m_color;//D3DXVECTOR4(0.14f, 0.34f, 0.60f, 1.0f);
	vertices[7].normal = D3DXVECTOR3(0.0f, 1.0f, 0.0f);

	// Load the index array with data.
	indices[0] = 3;  
	indices[1] = 1;  
	indices[2] = 0;  

	indices[3] = 2;  
	indices[4] = 1;  
	indices[5] = 3;  

	indices[6] = 0;  
	indices[7] = 5;  
	indices[8] = 4; 

	indices[9] = 1;  
	indices[10] = 5;  
	indices[11] = 0;  

	indices[12] = 3;  
	indices[13] = 4;  
	indices[14] = 7;  

	indices[15] = 0;  
	indices[16] = 4;  
	indices[17] = 3;  

	indices[18] = 1; 
	indices[19] = 6;  
	indices[20] = 5;  

	indices[21] = 2;  
	indices[22] = 6;  
	indices[23] = 1; 

	indices[24] = 2;  
	indices[25] = 7; 
	indices[26] = 6;

	indices[27] = 3; 
	indices[28] = 7;  
	indices[29] = 2;  

	indices[30] = 6;  
	indices[31] = 4;  
	indices[32] = 5;  

	indices[33] = 7;  
	indices[34] = 4;  
	indices[35] = 6;  


	// Set up the description of the static vertex buffer.
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexType) * m_vertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if(FAILED(result))
		return false;

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_indexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if(FAILED(result))
		return false;

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete [] vertices;
	vertices = 0;

	delete [] indices;
	indices = 0;

	return true;
}


void ModelClass::ShutdownBuffers()
{
	// Release the index buffer.
	if(m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = 0;
	}

	// Release the vertex buffer.
	if(m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = 0;
	}

	return;
}


void ModelClass::RenderBuffers(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride;
	unsigned int offset;


	// Set vertex buffer stride and offset.
	stride = sizeof(VertexType); 
	offset = 0;

	//D3DXMATRIX scaleMatrix;
	//D3DXMATRIX translateMatrix;
	//D3DXMATRIX rotationMatrix;

	//D3DXMatrixScaling(&scaleMatrix, 1.0f, 0.5f, 0.5f);
	//D3DXMatrixRotationY(&rotationMatrix, -1);
	////D3DXMatrixTranslation(&translateMatrix, m_positionX, m_positionY * 0.5f, m_positionZ);
	//D3DXMatrixTranslation(&translateMatrix, m_positionX, m_positionY * 0.5f, m_positionZ);

	//m_modelMatrix = scaleMatrix * rotationMatrix * translateMatrix;
	//worldMatrix *= translation;
    
	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	return;
}




//void ModelClass::getMatrix(D3DXMATRIX& worldMatrix)
//{
//	worldMatrix = m_modelMatrix;
//	return;
//}


//void ModelClass::SetFrameTime(float time)
//{
//	m_frameTime = time;
//	return;
//}


//void ModelClass::Move()
//{
//	m_forwardSpeed += m_frameTime * 0.000001f;
//	m_positionX += m_forwardSpeed;
//	return;
//}

//void ModelClass::SetPosition(float x, float y, float z)
//{
//	m_positionX = x;
//	m_positionY = y;
//	m_positionZ = z;
//	return;
//}


bool ModelClass::LoadModel(char* filename)
{
	ifstream fin;
	char input;
	int i;


	// Open the model file.
	fin.open(filename);
	
	// If it could not open the file then exit.
	if(fin.fail())
	{
		return false;
	}

	// Read up to the value of vertex count.
	fin.get(input);
	while(input != ':')
	{
		fin.get(input);
	}

	// Read in the vertex count.
	fin >> m_vertexCount;

	// Set the number of indices to be the same as the vertex count.
	m_indexCount = m_vertexCount;

	// Create the model using the vertex count that was read in.
	m_model = new ModelType[m_vertexCount];
	if(!m_model)
	{
		return false;
	}

	// Read up to the beginning of the data.
	fin.get(input);
	while(input != ':')
	{
		fin.get(input);
	}
	fin.get(input);
	fin.get(input);

	// Read in the vertex data.
	for(i=0; i<m_vertexCount; i++)
	{
		fin >> m_model[i].x >> m_model[i].y >> m_model[i].z;
		fin >> m_model[i].tu >> m_model[i].tv;
		fin >> m_model[i].nx >> m_model[i].ny >> m_model[i].nz;
	}

	// Close the model file.
	fin.close();

	return true;
}

// The ReleaseModel function handles deleting the model data array.

void ModelClass::ReleaseModel()
{
	if(m_model)
	{
		delete [] m_model;
		m_model = 0;
	}

	return;
}




void ModelClass::SetColor(D3DXVECTOR4 color)
{
	m_color = color;
}

//void ModelClass::SetTrack(TrackClass t)
//{
//	m_track = t;
//}
//
//void ModelClass::SetTrackPosition(int p)
//{
//	m_trackPosition = p;
//}