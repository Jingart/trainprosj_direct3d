#pragma once
#include "trainobject.h"
#include "locomotiveobject.h"
#include "traincartclass.h"

#include "colorshader.h"
#include "d3dclass.h"
#include "light.h"

#include <vector>

using namespace std;

class Train
{
public:
	Train(void);
	//TrainClass(ColorShaderClass* colorShader);
	~Train(void);
	
	bool Initialize(ID3D11Device*);
	void Shutdown();

	void SetTrack(TrackClass*);
	void MoveToDestination();
	void SetFrameTime(float);

	void Render(ColorShaderClass* colorShader, D3DClass* direct3D, LightClass* light, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix);

private:
	//vector<TrainObject*> m_trainobjects;
	LocomotiveObject* m_locomotive;
	vector<TrainCartClass*> m_trainobjects;

	TrackClass* m_TestTrack;

};

