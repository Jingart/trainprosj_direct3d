////////////////////////////////////////////////////////////////////////////////
// Filename: modelclass.h
////////////////////////////////////////////////////////////////////////////////
#pragma once
#ifndef _MODELCLASS_H_
#define _MODELCLASS_H_


//////////////
// INCLUDES //
//////////////
#include <d3d11.h>
#include <d3dx10math.h>
#include <fstream>


using namespace std;
///////////////////////
// MY CLASS INCLUDES //
///////////////////////
//#include "textureclass.h"

////////////////////////////////////////////////////////////////////////////////
// Class name: ModelClass
////////////////////////////////////////////////////////////////////////////////
class ModelClass
{
private:

	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR4 color;
		D3DXVECTOR3 normal;
	};

	struct ModelType
	{
		float x, y, z;
		float tu, tv;
		float nx, ny, nz;
	};

public:

	ModelClass();
	ModelClass(float x, float y, float z);
	ModelClass(const ModelClass&);
	~ModelClass();
	bool Initialize(ID3D11Device*);
	bool Initialize(ID3D11Device*, char*);
	void Shutdown();
	void Render(ID3D11DeviceContext*);
	int GetIndexCount();
	void SetColor(D3DXVECTOR4 color);

	//void MoveQ(bool keydown);
	//float m_frameTime;
	//float m_forwardSpeed;

    //void getMatrix(D3DXMATRIX& worldMatrix);
	//void Move();
	//void SetFrameTime(float time);
	//void SetPosition(float x, float y, float z);
	void ShutdownBuffers();

	//void SetTrackPosition(int);
	//void SetTrack(TrackClass);
	//void MoveForwardOnTrack();


protected:

	bool InitializeBuffers(ID3D11Device*);
	bool InitializeBuffers2(ID3D11Device*);
	
	void RenderBuffers(ID3D11DeviceContext*);

	//float m_positionX;
	//float m_positionY;
	//float m_positionZ;

	bool LoadModel(char*);
	void ReleaseModel();

protected:

	ID3D11Buffer *m_vertexBuffer; 
	ID3D11Buffer *m_indexBuffer;
	int m_vertexCount;
	int m_indexCount;
	//D3DXMATRIX m_modelMatrix;
	ModelType* m_model;
	D3DXVECTOR4 m_color;
	//TrackClass m_track;
	//m_trackPosition;
};

#endif