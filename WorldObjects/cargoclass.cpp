#include "cargoclass.h"


//CargoClass::CargoClass(void)
//{
//}
CargoClass::CargoClass(void)
	: ModelObject()
{

	m_positionX = 0.0f;
	m_positionY = 0.0f;
	m_positionZ = 0.0f;

	m_scaleX = 0.1f;
	m_scaleY = 0.1f;
	m_scaleZ = 0.1f;

	m_color = D3DXVECTOR4(0.8f, 0.4f, 0.20f, 1.0f);

}


void CargoClass::SetCargoType(CargoEnum type)
{
	m_cargoType = type;
}
