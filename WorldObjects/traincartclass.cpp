#include "traincartclass.h"


TrainCartClass::TrainCartClass(void)
	: TrainObject()
{
	//m_cargoList. ;
	m_cargoCapacity = 6;
	m_trainObjectType = CART_BOX;
}


TrainCartClass::~TrainCartClass(void)
{
}


void TrainCartClass::Shutdown()
{

	for (int i = 0; i < m_cargoList.size(); i++)
		m_cargoList[i]->Shutdown();

}


//void TrainCartClass::RenderCargo()
//{
//	for(int i = 0; i < m_cargoList.size(); i++)
//	{
//		//cargoList[i]->SetPosition();
//		//cargoList[i]->Render(
//	}
//}


void TrainCartClass::AddCargo(CargoClass* cargo)
{
	//if (m_cargoList.size() <= m_cargoCapacity)
	//cargo->SetPosition(m_positionX, m_positionY + 0.5f, m_positionZ);

	m_cargoList.push_back(cargo);
}


vector<CargoClass*> TrainCartClass::GetCargoVector()
{
	return m_cargoList;
}

//
void TrainCartClass::MoveToDestination()
{
	TrainObject::MoveToDestination();

	for (int i = 0; i < m_cargoList.size(); i++)
	{
		m_cargoList[i]->SetPosition(m_positionX, m_positionY + 3.0f, m_positionZ);
		m_cargoList[i]->SetRotation(m_rotationX, m_rotationY, m_rotationZ);
	}
}

void TrainCartClass::SetTrackPosition(int pos)
{
	TrainObject::SetTrackPosition(pos);

	for (int i = 0; i < m_cargoList.size(); i++)
	{
		m_cargoList[i]->SetPosition(m_positionX, m_positionY + 3.0f, m_positionZ);
	}
}