#include "train.h"


Train::Train(void)
{
		//vector<TileCoordinat> track;
		//TileCoordinat t;

		//t.x = 10;
		//t.y = 10;
		//track.push_back(t);

	//TrainObject *train = new TrainObject();
	

}




Train::~Train(void)
{

}


bool Train::Initialize(ID3D11Device* device)
{
	bool result;

	m_locomotive = new LocomotiveObject();
	m_trainobjects.push_back(new TrainCartClass()); 
	m_trainobjects.push_back(new TrainCartClass()); 
	//m_trainobjects.push_back(new TrainCartClass()); 
	//m_trainobjects.push_back(new TrainCartClass()); 



	m_locomotive->Initialize(device);

	for(int i = 0; i < m_trainobjects.size(); i++)
	{
		result = m_trainobjects[i]->Initialize(device);
		if(!result)
			return false;
	}


	CargoClass *cargo = new CargoClass();
	cargo->Initialize(device);
	m_trainobjects[0]->AddCargo(cargo);

	//CargoClass *cargo2 = new CargoClass();
	//cargo2->Initialize(device);
	//m_trainobjects[1]->AddCargo(cargo2);


	return true;
}

void Train::Shutdown()
{
	m_locomotive->Shutdown();

	for(int i = 0; i < m_trainobjects.size(); i++)
		m_trainobjects[i]->Shutdown();
}

void Train::SetTrack(TrackClass* trackObject)
{
	m_TestTrack = trackObject;

	int sPos = 5;
	int ePos = 166;

	m_locomotive->SetTrack(m_TestTrack);
	m_locomotive->SetTrackPosition(sPos);
	m_locomotive->SetTrackDestination(ePos);

	for(int i = 0; i < m_trainobjects.size(); i++)
	{
		m_trainobjects[i]->SetTrack(m_TestTrack);
		m_trainobjects[i]->SetTrackPosition(sPos - i - 1);
		m_trainobjects[i]->SetTrackDestination(ePos - i - 1);
	}

	
}

void Train::MoveToDestination()
{
	m_locomotive->MoveToDestination();

	for(int i = 0; i < m_trainobjects.size(); i++)
		m_trainobjects[i]->MoveToDestination();
}

void Train::SetFrameTime(float time)
{
	m_locomotive->SetFrameTime(time);

	for(int i = 0; i < m_trainobjects.size(); i++)
		m_trainobjects[i]->SetFrameTime(time);

	return;
}

void Train::Render(ColorShaderClass* colorShader, D3DClass* direct3D, LightClass* light, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix)
{
	bool result;
	//m_trainobjects[1]->Render();
	//m_trainobjects[1]->Render();

	D3DXMATRIX modelWorldMatrix;

	m_locomotive->GetMatrix(modelWorldMatrix);
	m_locomotive->Render(direct3D->GetDeviceContext());
	result = colorShader->Render(direct3D->GetDeviceContext(), m_locomotive->GetIndexCount(), modelWorldMatrix, viewMatrix, projectionMatrix, 
					light->GetDirection(), light->GetAmbientColor(), light->GetDiffuseColor());

	for(int i = 0; i < m_trainobjects.size(); i++)
	{

		vector<CargoClass*> m_cargoList = m_trainobjects[i]->GetCargoVector();

		for (int i = 0; i < m_cargoList.size(); i++)
		{
			m_cargoList[i]->GetMatrix(modelWorldMatrix);
			m_cargoList[i]->Render(direct3D->GetDeviceContext());
			result = colorShader->Render(direct3D->GetDeviceContext(), m_cargoList[i]->GetIndexCount(), modelWorldMatrix, viewMatrix, projectionMatrix, 
							light->GetDirection(), light->GetAmbientColor(), light->GetDiffuseColor());
		}

		m_trainobjects[i]->GetMatrix(modelWorldMatrix);
		m_trainobjects[i]->Render(direct3D->GetDeviceContext());
		result = colorShader->Render(direct3D->GetDeviceContext(), m_trainobjects[i]->GetIndexCount(), modelWorldMatrix, viewMatrix, projectionMatrix, 
						   light->GetDirection(), light->GetAmbientColor(), light->GetDiffuseColor());
	}

}
