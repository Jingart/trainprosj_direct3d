#pragma once

#include "modelobject.h"
#include "trackclass.h"
#include <vector>


class TrainObject : public ModelObject
{
public:
	enum  TrainObjectEnum{ LOCOMOTIVE, CART_BOX, CART_COAL};
	TrainObject(void);
	//~trainobject(void);

	
	void SetTrack(TrackClass*);
	void SetTrackPosition(int);
	void SetTrackDestination(int);

	int GetTrackPosition();
	TrainObjectEnum GetTrainObjectType();

	void MoveForwardOnTrack();
	void MoveToDestination();

	void MoveForward();
	bool m_canMove;
protected:

	TrackClass* m_TrackObject;
	vector<TrackClass::TileCoordinat> m_track;
    int m_trackPosition;
	int m_destination;
	TrainObjectEnum m_trainObjectType;
	
};

