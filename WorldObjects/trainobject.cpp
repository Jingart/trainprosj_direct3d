#include "trainobject.h"


TrainObject::TrainObject(void)
{

	m_positionX = 0.0f;
	m_positionY = 1.0f;
	m_positionZ = 0.0f;

	m_scaleX = 0.15f;
	m_scaleY = 0.15f;
	m_scaleZ = 0.4f;

	m_canMove = false;
	m_trackPosition = -1;
	m_destination = -1;
}
//
//
//trainobject::~trainobject(void)
//{
//}

void TrainObject::SetTrackPosition(int trackPosition)
{
	m_trackPosition = trackPosition;

	//vector<TrackClass::TileCoordinat> track;
	//m_TrackObject->GetTrack(track);

	m_positionX = m_track[m_trackPosition].x + 0.5f;
	//m_positionY = 0.0f;
	m_positionZ = m_track[m_trackPosition].y + 0.5f;

	//m_rotationY = 180;
}


void TrainObject::SetTrack(TrackClass* trackObject)
{
	m_TrackObject = trackObject;
	m_TrackObject->GetTrack(m_track);
}


void TrainObject::SetTrackDestination(int destination)
{
	m_destination = destination;
}


void TrainObject::MoveForwardOnTrack()
{
	//m_track[m_trackPosition];
}


void TrainObject::MoveForward()
{
	m_forwardSpeed += m_frameTime * 0.0000001f;
	float radians = m_rotationY * 0.0174532925f;
	m_positionX += sinf(radians) * m_forwardSpeed;
	m_positionZ += cosf(radians) * m_forwardSpeed;
	//m_positionX += m_forwardSpeed;
	return;
}


void TrainObject::MoveToDestination()
{

	// viss ikkje framme eller dest ikkje er n�dd
	if (m_destination != -1 && m_trackPosition < m_destination)
	{


		int currentTile = m_trackPosition;
		int nextTile = m_trackPosition + 1;
		int nextNextTile = m_trackPosition + 2;

		float currentTileX = m_track[currentTile].x + 0.5f;
		float currentTileY = m_track[currentTile].y + 0.5f;
		float nextTileX = m_track[nextTile].x + 0.5f;
		float nextTileY = m_track[nextTile].y + 0.5f;

		

		if(m_rotationY == 0)
		{
			// if neste tile g�r skr� opp h�gre
			if (currentTileX + 1 == nextTileX && currentTileY + 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 45;
			} // // if neste tile g�r skr� opp venstre
			else if (currentTileX - 1 == nextTileX && currentTileY + 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 315;
			} // if ein tile rett fram
			else if (false)
			{
				m_forwardSpeed = 0;
				m_canMove = false;
			}
		}

		else if(m_rotationY == 45)
		{
			// if neste tile g�r h�gre
			if (currentTileX + 1 == nextTileX && currentTileY == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 90;
			} // // if neste tile g�r opp
			else if (currentTileX == nextTileX && currentTileY + 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 0;
			} // if ein tile rett fram
			else if (false)
			{
				m_forwardSpeed = 0;
				m_canMove = false;
			}
		}

		else if(m_rotationY == 90)
		{
			// if neste tile g�r h�gre ned 
			if (currentTileX + 1 == nextTileX && currentTileY - 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 135;
			} // // if neste tile h�gre opp
			else if (currentTileX + 1 == nextTileX && currentTileY + 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 45;
			} // if ein tile rett fram
			else if (false)
			{
				m_forwardSpeed = 0;
				m_canMove = false;
			}
		}

		else if(m_rotationY == 135)
		{
			// if neste tile g�r ned 
			if (currentTileX == nextTileX && currentTileY - 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 180;
			} // // if neste tile h�gre
			else if (currentTileX + 1 == nextTileX && currentTileY == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 90;
			} // if ein tile rett fram
			else if (false)
			{
				m_forwardSpeed = 0;
				m_canMove = false;
			}
		}

		else if(m_rotationY == 180)
		{
			// if neste tile g�r h�gre ned 
			if (currentTileX + 1 == nextTileX && currentTileY - 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 135;
			} // // if neste tile venstre ned
			else if (currentTileX - 1 == nextTileX && currentTileY - 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 225;
			} // if ein tile rett fram
			else if (false)
			{
				m_forwardSpeed = 0;
				m_canMove = false;
			}
		}

		else if(m_rotationY == 225)
		{
			// if neste tile g�r venstre
			if (currentTileX - 1 == nextTileX && currentTileY == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 270;
			} // // if neste tile ned
			else if (currentTileX  == nextTileX && currentTileY - 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 180;
			} // if ein tile rett fram
			else if (false)
			{
				m_forwardSpeed = 0;
				m_canMove = false;
			}
		}

		else if(m_rotationY == 270)
		{
			// if neste tile g�r venstre ned
			if (currentTileX - 1 == nextTileX && currentTileY - 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 225;
			} // // if neste tile g�r venstre opp
			else if (currentTileX - 1  == nextTileX && currentTileY + 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 315;
			} // if ein tile rett fram
			else if (false)
			{
				m_forwardSpeed = 0;
				m_canMove = false;
			}
		}

		else if(m_rotationY == 315)
		{
			// if neste tile g�r opp
			if (currentTileX == nextTileX && currentTileY + 1 == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 0;
			} // // if neste tile g�r venstre
			else if (currentTileX - 1  == nextTileX && currentTileY == nextTileY)
			{
					m_positionX = (float)currentTileX;
					m_positionZ = (float)currentTileY;
					m_rotationY = 270;
			} // if ein tile rett fram
			else if (false)
			{
				m_forwardSpeed = 0;
				m_canMove = false;
			}
		}

		if (m_forwardSpeed <  0.0007f) 
			m_forwardSpeed += m_frameTime * 0.000001f;
		//if(m_forwardSpeed > 0.0007f)
		//m_forwardSpeed = 0.0009f;
		//m_forwardSpeed += m_frameTime + 0.0000000000001f;
		//m_forwardSpeed = 0.0003f;
		float radians = m_rotationY * 0.0174532925f;
		m_positionX += sinf(radians) * m_forwardSpeed;
		m_positionZ += cosf(radians) * m_forwardSpeed;


		if (m_positionZ < (float)nextTileY + 0.02f && m_positionZ > (float)nextTileY - 0.02f && m_positionX < (float)nextTileX + 0.02f && m_positionX > (float)nextTileX - 0.02f)
			m_trackPosition++;

	}else
	{
		m_forwardSpeed = 0;
		m_canMove = false;
	}


	return;
}


int TrainObject::GetTrackPosition()
{
	return m_trackPosition;
}

//
//TrainObjectEnum TrainObject::GetTrainObjectType()
//{
//
//
//}



























/*else
		{
			m_forwardSpeed = 0;
				m_canMove = false;
		}*/

		//if (m_rotationY == 0)
		//{
		//	if (m_positionZ >= (float)nextTileY && (m_positionX == (float)currentTileX))
		//	{
		//		m_trackPosition++;
		//	}
		//}

		//else if (m_rotationY == 45)
		//{
		//	if (m_positionZ >= (float)nextTileY && m_positionX == (float)nextTileX)
		//	{
		//		m_trackPosition++;
		//	}
		//}

		//else if (m_rotationY == 90)
		//{
		//	if (m_positionZ == (float)currentTileY && m_positionX >= (float)nextTileX)
		//	{
		//		m_trackPosition++;
		//	}
		//}





		//int currentTile = m_trackPosition;
		//int nextTile = m_trackPosition + 1;
		//int nextNextTile = m_trackPosition + 2;
		//m_rotationY = 180;
		//// opp
		//if(m_rotationY == 0)
		//{
		//	// if neste tile ligg rett til h�gre
		//	if (m_track[currentTile].x + 1 == m_track[nextTile].x && m_track[currentTile].y == m_track[nextTile].y)
		//	{
		//		//If nesteneste tile ligg oppafor neste tile
		//		if ( m_track[nextTile].x == m_track[nextNextTile].x && m_track[nextTile].y + 1 == m_track[nextNextTile].y)
		//			m_rotationY = 45;

		//	} // if neste tile ligg rett til venstre
		//	else if (m_track[currentTile].x - 1 == m_track[nextTile].x && m_track[currentTile].y == m_track[nextTile].y)
		//	{
		//		//If nesteneste tile ligg oppafor neste tile
		//		if ( m_track[nextTile].x == m_track[nextNextTile].x && m_track[nextTile].y + 1 == m_track[nextNextTile].y)
		//			m_rotationY = 315;
		//	} // if ein tile rett fram
		//	else if (m_track[currentTile].x == m_track[nextTile].x && m_track[currentTile].y + 1 == m_track[nextTile].y)
		//	{
		//	}
		//	// stopp
		//	else
		//	{
		//		m_forwardSpeed = 0;
		//		m_canMove = false;
		//	}
		//}
		////opph�gre
		//else if (m_rotationY == 45)
		//{
		//	//Viss neste tile ligg rett til h�gre 
		//	if (m_track[currentTile].x + 1 == m_track[nextTile].x && m_track[currentTile].y && m_track[nextTile].y)
		//	{
		//		// og ikkje forset opph�gre
		//		if (m_track[nextTile].y + 1 != m_track[nextNextTile].y)
		//			m_rotationY = 90;
		//	} 
		//	// Viss neste tile ligg rett opp 
		//	else if (m_track[currentTile].x == m_track[nextTile].x && m_track[currentTile].y + 1 && m_track[nextTile].y)
		//	{
		//		m_rotationY = 0;
		//	}
		//	else // stop
		//	{
		//		m_forwardSpeed = 0;
		//		m_canMove = false;
		//	}
		//}
		////h�gre
		//else if (m_rotationY == 90)
		//{
		//	// if neste tile ligg rett ned
		//	if (m_track[currentTile].y - 1 == m_track[nextTile].y && m_track[currentTile].x == m_track[nextTile].x)
		//	{
		//		//If nesteneste tile ligg h�gre for neste tile
		//		if ( m_track[nextTile].x + 1 == m_track[nextNextTile].x) //&& m_track[nextTile].y == m_track[nextNextTile].y)
		//			m_rotationY = 135;
		//	}
		//	// if neste tile ligg rett opp
		//	else if (m_track[currentTile].y + 1 == m_track[nextTile].y && m_track[currentTile].x == m_track[nextTile].x)
		//	{
		//		//If nesteneste tile ligg h�gre for neste tile
		//		if ( m_track[nextTile].x + 1 == m_track[nextNextTile].x) //&& m_track[nextTile].y == m_track[nextNextTile].y)
		//			m_rotationY = 45;
		//	}
		//	else if (m_track[currentTile].x + 1 == m_track[nextTile].x && m_track[currentTile].y == m_track[nextTile].y)
		//	{
		//	}
		//	else // stop
		//	{
		//		m_forwardSpeed = 0;
		//		m_canMove = false;
		//	}
		//}
		////nedh�gre
		//else if (m_rotationY == 135)
		//{
		//	// if neste tile ligg rett ned
		//	if (m_track[currentTile].y - 1 == m_track[nextTile].y && m_track[currentTile].x == m_track[nextTile].x)
		//	{
		//		// og ikkje forset nedh�gre
		//		if (m_track[nextTile].x + 1 != m_track[nextNextTile].x)
		//			m_rotationY = 180;
		//	}
		//}
		//// ned
		//if(m_rotationY == 180)
		//{
		//	// if neste tile ligg rett til h�gre
		//	if (m_track[currentTile].x + 1 == m_track[nextTile].x && m_track[currentTile].y == m_track[nextTile].y)
		//	{
		//		//If nesteneste tile ligg oppafor neste tile
		//		if ( m_track[nextTile].x == m_track[nextNextTile].x && m_track[nextTile].y - 1 == m_track[nextNextTile].y)
		//			m_rotationY = 135;
		//	} // if neste tile ligg rett til venstre
		//	if (m_track[currentTile].x - 1 == m_track[nextTile].x && m_track[currentTile].y == m_track[nextTile].y)
		//	{
		//		//If nesteneste tile ligg neafor neste tile
		//		if ( m_track[nextTile].x == m_track[nextNextTile].x && m_track[nextTile].y - 1 == m_track[nextNextTile].y)
		//			m_rotationY = 225;
		//	} // if ein tile rett fram
		//	else if (m_track[currentTile].x == m_track[nextTile].x && m_track[currentTile].y - 1 == m_track[nextTile].y)
		//	{
		//	}
		//	 //stopp
		//	else
		//	{
		//		m_forwardSpeed = 0;
		//		m_canMove = false;
		//	}
		//}

		////nedh�gre
		//else if (m_rotationY == 225)
		//{
		//	// if neste tile ligg rett til venstre
		//	if (m_track[currentTile].x - 1 == m_track[nextTile].y && m_track[currentTile].y == m_track[nextTile].y)
		//	{
		//		// og ikkje forset nedh�gre
		//		if (m_track[nextTile].x + 1 != m_track[nextNextTile].x)
		//			m_rotationY = 270;
		//	}
		//}

		//m_forwardSpeed += m_frameTime * 0.000001f;
		//if(m_forwardSpeed > 0.1f)
		//	m_forwardSpeed = 0.1f;
		////m_forwardSpeed += m_frameTime + 0.0000000000001f;
		////m_forwardSpeed = 0.0003f;
		//float radians = m_rotationY * 0.0174532925f;
		//m_positionX += sinf(radians) * m_forwardSpeed;
		//m_positionZ += cosf(radians) * m_forwardSpeed;


		//if (m_rotationY == 0)
		//{
		//	if (m_positionZ > m_track[currentTile].y + 1 && (m_positionX > m_track[currentTile].x && m_positionX < m_track[currentTile].x + 1))
		//	{
		//		m_trackPosition++;
		//	}
		//}

		//else if (m_rotationY == 45)
		//{
		//	if ((m_positionX > m_track[nextNextTile].x)
		//		&& (m_positionZ >= m_track[nextNextTile].y + 0.5f))
		//	{
		//		m_trackPosition += 2;
		//	}
		//}

		//else if (m_rotationY == 90)
		//{
		//	// viss neste tile ha x + 1 og y til neste tile er p� linje med current tile
		//	if (m_positionX >= m_track[currentTile].x + 1 && (m_positionZ >= m_track[currentTile].y && m_positionZ <= m_track[currentTile].y + 1))
		//	{
		//		m_trackPosition++;
		//	}
		//}

		//else if (m_rotationY == 135)
		//{
		//	if ((m_positionX > m_track[nextNextTile].x)
		//		&& (m_positionZ < m_track[nextNextTile].y))
		//	{
		//		m_trackPosition += 2;
		//	}
		//}

		//else if (m_rotationY == 180)
		//{
		//	if (m_positionZ < m_track[currentTile].y && (m_positionX > m_track[currentTile].x && m_positionX < m_track[currentTile].x + 1))
		//	{
		//		m_trackPosition++;
		//	}
		//}

		//else if (m_rotationY == 225)
		//{
		//	if ((m_positionX < m_track[nextNextTile].x - 1)
		//		&& (m_positionZ < m_track[nextNextTile].y + 1))
		//	{
		//		m_trackPosition += 2;
		//	}
		//}
