#pragma once
#ifndef _TRACKCLASS_H_
#define _TRACKCLASS_H_


//////////////
// INCLUDES //
//////////////
#include <d3d11.h>
#include <d3dx10math.h>
#include <vector>


using namespace std;

class TrackClass
{

private:

	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR3 normal;
		D3DXVECTOR4 color;
	};

public:
	
    struct TileCoordinat
	{
		int x;
	    int y;
	};

	TrackClass(void);
	TrackClass(const TrackClass&);
	~TrackClass(void);

	bool Initialize(ID3D11Device*);
	void Shutdown();
	void Render(ID3D11DeviceContext*);
	int GetIndexCount();
    void GetTrack(vector<TileCoordinat>&);

private:

	bool InitializeBuffers(ID3D11Device*);
	void ShutdownBuffers();
	void RenderBuffers(ID3D11DeviceContext*);

	int m_vertexCount;
	int m_indexCount;
	ID3D11Buffer *m_vertexBuffer; 
	ID3D11Buffer *m_indexBuffer;
	TileCoordinat asd;
	vector<TileCoordinat> track;

};

#endif