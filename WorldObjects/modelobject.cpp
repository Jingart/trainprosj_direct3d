#include "modelobject.h"


ModelObject::ModelObject(void)
{
	m_Model = 0;

	m_positionX = 0.0f;
	m_positionY = 0.0f;
	m_positionZ = 0.0f;

	m_rotationX = 0.0f;
	m_rotationY = 0.0f;
	m_rotationZ = 0.0f;

	m_scaleX = 1.0f;
	m_scaleY = 1.0f;
	m_scaleZ = 1.0f;

	m_frameTime = 0.0f;
	m_forwardSpeed = 0.0f;

	m_color = D3DXVECTOR4(0.14f, 0.34f, 0.60f, 1.0f);

}


ModelObject::~ModelObject(void)
{
}


bool ModelObject::Initialize(ID3D11Device* device)
{
	
	m_Model = new ModelClass;
	if(!m_Model)
		return false;

	SetColor(m_color);
	
	if(!m_Model->Initialize(device, "cube.txt"))
		return false;	
}


void ModelObject::Shutdown()
{

	// Release the model object.
	if(m_Model)
	{
		m_Model->Shutdown();
		delete m_Model;
		m_Model = 0;
	}

}


void ModelObject::GetMatrix(D3DXMATRIX& modelMatrix)
{
	D3DXMatrixScaling(&m_scaleMatrix, m_scaleX, m_scaleY, m_scaleZ);
	float radians = m_rotationY * 0.0174532925f;
	D3DXMatrixRotationY(&m_rotationMatrix, radians);
	D3DXMatrixTranslation(&m_translateMatrix, m_positionX, m_positionY * m_scaleY, m_positionZ);

	modelMatrix = m_scaleMatrix * m_rotationMatrix * m_translateMatrix;
}


void ModelObject::Render(ID3D11DeviceContext* deviceContext)
{
	m_Model->Render(deviceContext);
	return;
}


int ModelObject::GetIndexCount()
{
	return m_Model->GetIndexCount();
}


void ModelObject::SetFrameTime(float time)
{
	m_frameTime = time;
	return;
}


void ModelObject::SetScale(float scaleX, float scaleY, float scaleZ)
{
	m_scaleX = scaleX;
	m_scaleY = scaleY;
	m_scaleZ = scaleZ;
}


void ModelObject::SetRotation(float rotationX, float rotationY, float rotationZ)
{
	m_rotationX = rotationX;
	m_rotationY = rotationY;
	m_rotationZ = rotationZ;
}


void ModelObject::SetPosition(float positionX, float positionY, float positionZ)
{
	m_positionX = positionX;
	m_positionY = positionY;
	m_positionZ = positionZ;
}

void ModelObject::SetColor(D3DXVECTOR4 color)
{
	m_Model->SetColor(color);
}

float ModelObject::GetPostitionX()
{
	return m_positionX;
}


float ModelObject::GetPostitionY()
{
	return m_positionY;
}


float ModelObject::GetPostitionZ()
{
	return m_positionZ;
}


float ModelObject::GetRotationX()
{
	return m_rotationX;
}


float ModelObject::GetRotationY()
{
	return m_rotationY;
}


float ModelObject::GetRotationZ()
{
	return m_rotationZ;
}


float ModelObject::GetSpeed()
{
	return m_forwardSpeed;;
}

