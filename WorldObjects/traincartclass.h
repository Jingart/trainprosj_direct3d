#pragma once
#include "trainobject.h"
#include "cargoclass.h"
#include <vector>


class TrainCartClass : public TrainObject
{
public:
	
	TrainCartClass(void);
	~TrainCartClass(void);
	void Shutdown();

	void RenderCargo();

	vector<CargoClass*> GetCargoVector();
	void AddCargo(CargoClass* cargo);
	void RemoveCargo();
	void MoveToDestination();
	void SetTrackPosition(int);

	//Set
private:
	vector<CargoClass*> m_cargoList;
	int m_cargoCapacity;
};

