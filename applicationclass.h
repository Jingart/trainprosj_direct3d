////////////////////////////////////////////////////////////////////////////////
// Filename: applicationclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _APPLICATIONCLASS_H_
#define _APPLICATIONCLASS_H_


/////////////
// GLOBALS //
/////////////
const bool FULL_SCREEN = true;
const bool VSYNC_ENABLED = true;
const float SCREEN_DEPTH = 1000.0f;
const float SCREEN_NEAR = 0.1f;


///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include "input.h"
#include "d3dclass.h"
#include "cameraclass.h"
#include "terraincolorclass.h"
#include "colorshader.h"
#include "timerclass.h"
#include "positionclass.h"
#include "fpsclass.h"
#include "cpuclass.h"
#include "fontshader.h"
#include "textclass.h"
#include "train.h"
#include "trainstation.h"
#include "plantbox.h"
#include "plantcollection.h"
#include "tracksystem.h"

//#include "railroadtrack.h"
#include "RailRoad.h"

#include "light.h"


////////////////////////////////////////////////////////////////////////////////
// Class name: ApplicationClass
////////////////////////////////////////////////////////////////////////////////
class ApplicationClass
{
public:
	ApplicationClass();
	ApplicationClass(const ApplicationClass&);
	~ApplicationClass();

	bool Initialize(HINSTANCE, HWND, int, int);
	void Shutdown();
	bool Frame();

private:
	bool HandleInput(float);
	bool UpdateModelPositions(float);
	bool UpdateTimers(float frameTime);
	bool RenderGraphics();

	bool InitializeTerrain(HWND hwnd);
	bool InitializeInput(HWND, HINSTANCE hinstance, int screenWidth, int screenHeight);
	bool InitializeColorShader(HWND);
	bool InitializeTimer(HWND);
	bool InitializeCameraPosition();
	bool InitializeCamera();
	bool InitializeFontShader(HWND);
	bool InitializeText(HWND, int, int);
	bool InitializeLight();
	bool InitializeRailRoadTrack(HWND);
	bool InitializeWorldObjects(HWND);


private:
	InputClass* m_Input;
	D3DClass* m_Direct3D;
	TimerClass* m_Timer;
	
	TerrainColorClass* m_TerrainColor;
		
	CameraClass* m_Camera;
	PositionClass* m_Position;

	FpsClass* m_Fps;
	CpuClass* m_Cpu;
	TextClass* m_Text;

	ColorShaderClass* m_ColorShader;
	FontShaderClass* m_FontShader;
	LightClass* m_Light;

	Train* m_TrainTestClass;
	PlantCollection* m_plantCollection;

	TrackSystem* m_TrackSystem;
	/*
	//TODO testobjects
	TrainStation* m_trainStation;
	TrainStation* m_trainStation2;

	//RailRoadTrack* m_TestTrack;
	RailRoad* m_TestTrackNode;
	*/
	D3DXMATRIX m_baseViewMatrix;
};

#endif