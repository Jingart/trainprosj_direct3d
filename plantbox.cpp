#include "plantbox.h"


PlantBox::PlantBox(void)
	: Plant()
{

	m_positionX = 0.0f;
	m_positionY = 1.0f;
	m_positionZ = 0.0f;

	m_scaleX = 1.0f;
	m_scaleY = 0.5f;
	m_scaleZ = 1.5f;

	cargoArea.x = 0;
	cargoArea.y = 0;

	m_plantType = BOXPLANT;
	m_color = D3DXVECTOR4(0.4f, 0.4f, 0.4f, 1.0f);
}


PlantBox::PlantBox(float x, float y, float z)
{
	m_positionX = x;
	m_positionY = y;
	m_positionZ = z;

	m_scaleX = 1.0f;
	m_scaleY = 0.5f;
	m_scaleZ = 2.0f;

	cargoArea.x = 0;
	cargoArea.y = 0;

	m_plantType = BOXPLANT;
	m_color = D3DXVECTOR4(0.4f, 0.4f, 0.4f, 1.0f);
}


PlantBox::PlantBox(float x, float z, float rotation, D3DXVECTOR4 color)
{
	//TODO dynamic height
    m_positionX = x;
	m_positionY = 1.0f;
	m_positionZ = z;

	m_scaleX = 1.0f;
	m_scaleY = 0.5f;
	m_scaleZ = 2.0f;

	m_rotationY = rotation;

	//TODO no effect / initialize
	cargoArea.x = 0;
	cargoArea.y = 0;

	m_plantType = BOXPLANT;
	m_color = color;
}


PlantBox::~PlantBox(void)
{
}


bool PlantBox::Initialize(ID3D11Device* device)
{
	bool result;

	Plant::Initialize(device);


	if(m_rotationY == 0)
	{
		cargoArea.x = m_positionX + 1;
		cargoArea.y = m_positionZ;
	}
	else if(m_rotationY == 90)
	{
		cargoArea.x = m_positionX ;
		cargoArea.y = m_positionZ - 2;
	}



	//TODO - manual cargo creation
	CargoBox *cargo = new CargoBox();
	if(!cargo)
		return false;

	result = cargo->Initialize(device);
	if(!result)
		return false;

	//TODO - unify tilecentering?
	cargo->SetPosition(cargoArea.x + (1.0f * cargo->GetScaleX()), m_positionY, cargoArea.y  + (1.0f * cargo->GetScaleZ()));

	m_cargoShippingList.push_back(cargo);




	//TODO - manual cargo creation
	CargoBox *cargo2 = new CargoBox(D3DXVECTOR4(0.24f, 0.64f, 0.8f, 1.0f));
	if(!cargo)
		return false;

	result = cargo2->Initialize(device);
	if(!result)
		return false;

	//TODO - unify tilecentering?
	cargo2->SetPosition(cargoArea.x + (1.0f * cargo->GetScaleX()), m_positionY, cargoArea.y  + (1.0f * cargo->GetScaleZ()) + (2.0f * cargo->GetScaleZ()));
	m_cargoShippingList.push_back(cargo2);

	return true;
}


//void PlantBox::SetFrameTime(float time)
//{
//	Plant::SetFrameTime(time);
//	cargoArea.x = m_positionX;
//	cargoArea.y = m_positionZ;
//}


vector<CargoBox*>* PlantBox::GetCargoShippingList()
{
	return &m_cargoShippingList;
}

