////////////////////////////////////////////////////////////////////////////////
// Filename: applicationclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "applicationclass.h"


ApplicationClass::ApplicationClass()
{
	m_Input = 0;
	m_Direct3D = 0;
	m_Timer = 0;
	
	// Shaders
	m_TerrainColor = 0;
	m_ColorShader = 0;
	m_Light = 0;
	
	// Camera
	m_Camera = 0;
	m_Position = 0;

	// Interface
	m_Fps = 0;
	m_Cpu = 0;
	m_FontShader = 0;
	m_Text = 0;

	//m_TestTrackNode = 0;
	//m_trainStation = 0;
	//m_trainStation2 = 0;

	m_TrackSystem = 0;

	m_TrainTestClass = 0;
	m_plantCollection = 0;
}


ApplicationClass::ApplicationClass(const ApplicationClass& other)
{
}


ApplicationClass::~ApplicationClass()
{
}


bool ApplicationClass::Initialize(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight)
{
	bool result;
	
	D3DXMATRIX baseViewMatrix;

	// Create the Direct3D object.
	m_Direct3D = new D3DClass;
	if(!m_Direct3D)
		return false;

	// Initialize the Direct3D object.
	result = m_Direct3D->Initialize(screenWidth, screenHeight, VSYNC_ENABLED, hwnd, FULL_SCREEN, SCREEN_DEPTH, SCREEN_NEAR);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize DirectX 11.", L"Error", MB_OK);
		return false;
	}

	// Create the fps object.
	m_Fps = new FpsClass;
	if(!m_Fps)
		return false;

	// Initialize the fps object.
	m_Fps->Initialize();

	// Create the cpu object.
	m_Cpu = new CpuClass;
	if(!m_Cpu)
		return false;

	// Initialize the cpu object.
	m_Cpu->Initialize();

	InitializeTerrain(hwnd);
	InitializeInput(hwnd, hinstance, screenWidth, screenHeight);
	InitializeColorShader(hwnd);
	InitializeTimer(hwnd);
	InitializeCamera();
	InitializeFontShader(hwnd);
	InitializeText(hwnd, screenWidth, screenHeight);
	InitializeLight();
	InitializeRailRoadTrack(hwnd);
	InitializeWorldObjects(hwnd);

	return true;
}


bool ApplicationClass::InitializeTerrain(HWND hwnd)
{
	// Create the terrain object.
	m_TerrainColor = new TerrainColorClass;
	if(!m_TerrainColor)
		return false;

	// Initialize the terrain object.
	bool result = m_TerrainColor->Initialize(m_Direct3D->GetDevice());
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the terrain object.", L"Error", MB_OK);
		return false;
	}
}


bool ApplicationClass::InitializeInput(HWND hwnd, HINSTANCE hinstance, int screenWidth, int screenHeight)
{

	// Create the input object.  The input object will be used to handle reading the keyboard and mouse input from the user.
	m_Input = new InputClass;
	if(!m_Input)
		return false;

	// Initialize the input object.
	bool result = m_Input->Initialize(hinstance, hwnd, screenWidth, screenHeight);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the input object.", L"Error", MB_OK);
		return false;
	}

}


bool ApplicationClass::InitializeColorShader(HWND hwnd)
{

	// Create the color shader object.
	m_ColorShader = new ColorShaderClass;
	if(!m_ColorShader)
		return false;

	// Initialize the color shader object.
	bool result = m_ColorShader->Initialize(m_Direct3D->GetDevice(), hwnd);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the colors shader object.", L"Error", MB_OK);
		return false;
	}
}


bool ApplicationClass::InitializeTimer(HWND hwnd)
{
	// Create the timer object.
	m_Timer = new TimerClass;
	if(!m_Timer)
		return false;

	// Initialize the timer object.
	bool result = m_Timer->Initialize();
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the timer object.", L"Error", MB_OK);
		return false;
	}
}


bool ApplicationClass::InitializeCamera()
{
	float cameraX, cameraY, cameraZ;
	float cameraRX, cameraRY, cameraRZ;

	// Create the camera object.
	m_Camera = new CameraClass;
	if(!m_Camera)
		return false;

	// Initialize a base view matrix with the camera for 2D user interface rendering.
	m_Camera->SetPosition(0.0f, 0.0f, -1.0f);
	m_Camera->Render();
	m_Camera->GetViewMatrix(m_baseViewMatrix);

	// Set the initial position of the camera.
	cameraX = 0.0f;
	cameraY = 15.0f;
	cameraZ = -5.0f;

	m_Camera->SetPosition(cameraX, cameraY, cameraZ);

	cameraRX = 25.0f;
	cameraRY = 35.0f;
	cameraRZ = 0.0f;

	m_Camera->SetRotation(cameraRX, cameraRY, cameraRZ);

	// Create the position object.
	m_Position = new PositionClass;
	if(!m_Position)
		return false;

	// Set the initial position of the viewer to the same as the initial camera position.
	m_Position->SetPosition(cameraX, cameraY, cameraZ);
	m_Position->SetRotation(cameraRX, cameraRY, cameraRZ);
}


bool ApplicationClass::InitializeFontShader(HWND hwnd)
{
	// Create the font shader object.
	m_FontShader = new FontShaderClass;
	if(!m_FontShader)
		return false;

	// Initialize the font shader object.
	bool result = m_FontShader->Initialize(m_Direct3D->GetDevice(), hwnd);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the font shader object.", L"Error", MB_OK);
		return false;
	}

}


bool ApplicationClass::InitializeText(HWND hwnd, int screenWidth, int screenHeight)
{
	bool result;
	
	char videoCard[128];
	int videoMemory;

	// Create the text object.
	m_Text = new TextClass;
	if(!m_Text)
		return false;

	// Initialize the text object.
	result = m_Text->Initialize(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), hwnd, screenWidth, screenHeight, m_baseViewMatrix);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the text object.", L"Error", MB_OK);
		return false;
	}

	// Retrieve the video card information.
	m_Direct3D->GetVideoCardInfo(videoCard, videoMemory);

	// Set the video card information in the text object.
	result = m_Text->SetVideoCardInfo(videoCard, videoMemory, m_Direct3D->GetDeviceContext());
	if(!result)
	{
		MessageBox(hwnd, L"Could not set video card info in the text object.", L"Error", MB_OK);
		return false;
	}


}


bool ApplicationClass::InitializeLight()
{

	m_Light = new LightClass;
	if(!m_Light)
		return false;

	// Initialize the light object.
	m_Light->SetAmbientColor(0.30f, 0.30f, 0.30f, 1.0f);
	m_Light->SetDiffuseColor(1.0f, 1.0f, 1.0f, 1.0f);
	m_Light->SetDirection(0.0f, -0.5f, 1.0f);
}


bool ApplicationClass::InitializeRailRoadTrack(HWND hwnd)
{

	m_TrackSystem = new TrackSystem;
	if(!m_TrackSystem)
		return false;
	
	// Initialize the model object.
	bool result = m_TrackSystem->Initialize(m_Direct3D->GetDevice());
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the model object.", L"Error", MB_OK);
		return false;
	}

}


bool ApplicationClass::InitializeWorldObjects(HWND hwnd)
{

	bool result;

	//TODO placeholder
	// Create the model object.
	m_TrainTestClass = new Train;
	if(!m_TrainTestClass)
		return false;

	// Initialize the model object.
	result = m_TrainTestClass->Initialize(m_Direct3D->GetDevice());
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the model object.", L"Error", MB_OK);
		return false;
	}

	vector<RailRoad*> trackColl = m_TrackSystem->getRailroadTrackCollection();
	m_TrainTestClass->SetTrack(trackColl[0]);



	//TODO placeholder
	m_plantCollection = new PlantCollection;
	if(!m_plantCollection)
		return false;

	PlantBox* plantObj = new PlantBox(8.0f, 1.0f, 7.0f);
	if(!plantObj)
		return false;

	result = plantObj->Initialize(m_Direct3D->GetDevice());
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the model object.", L"Error", MB_OK);
		return false;
	}


	PlantBox* plantObj2 = new PlantBox(44.0f, 4.0f, 90.0f, D3DXVECTOR4(0.4f, 0.8f, 0.4f, 1.0f));
	if(!plantObj2)
		return false;

	result = plantObj2->Initialize(m_Direct3D->GetDevice());
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the model object.", L"Error", MB_OK);
		return false;
	}


	m_plantCollection->AddPlant(plantObj);
	m_plantCollection->AddPlant(plantObj2);

}


void ApplicationClass::Shutdown()
{
	// Release the text object.
	if(m_Text)
	{
		m_Text->Shutdown();
		delete m_Text;
		m_Text = 0;
	}

	// Release the font shader object.
	if(m_FontShader)
	{
		m_FontShader->Shutdown();
		delete m_FontShader;
		m_FontShader = 0;
	}

	// Release the cpu object.
	if(m_Cpu)
	{
		m_Cpu->Shutdown();
		delete m_Cpu;
		m_Cpu = 0;
	}

	// Release the fps object.
	if(m_Fps)
	{
		delete m_Fps;
		m_Fps = 0;
	}

	// Release the position object.
	if(m_Position)
	{
		delete m_Position;
		m_Position = 0;
	}

	// Release the timer object.
	if(m_Timer)
	{
		delete m_Timer;
		m_Timer = 0;
	}

	// Release the color shader object.
	if(m_ColorShader)
	{
		m_ColorShader->Shutdown();
		delete m_ColorShader;
		m_ColorShader = 0;
	}

	// Release the terrain object.
	if(m_TerrainColor)
	{
		m_TerrainColor->Shutdown();
		delete m_TerrainColor;
		m_TerrainColor = 0;
	}

	// Release the camera object.
	if(m_Camera)
	{
		delete m_Camera;
		m_Camera = 0;
	}

	// Release the Direct3D object.
	if(m_Direct3D)
	{
		m_Direct3D->Shutdown();
		delete m_Direct3D;
		m_Direct3D = 0;
	}

	// Release the input object.
	if(m_Input)
	{
		m_Input->Shutdown();
		delete m_Input;
		m_Input = 0;
	}

	if(m_Light)
	{
		delete m_Light;
		m_Light = 0;
	}

	if(m_TrainTestClass)
	{
		m_TrainTestClass->Shutdown();
		delete m_TrainTestClass;
		m_TrainTestClass = 0;
	}

	if(m_plantCollection)
	{
		m_plantCollection->Shutdown();
		delete m_plantCollection;
		m_plantCollection = 0;
	}

	//if(m_trainStation)
	//{
	//	delete m_trainStation;
	//	m_trainStation = 0;
	//}


	//if(m_trainStation2)
	//{
	//	delete m_trainStation;
	//	m_trainStation = 0;
	//}

	/*
	if(m_TestTrackNode)
	{
		delete m_TestTrackNode;
		m_TestTrackNode = 0;
	}
	*/

	if(m_TrackSystem)
	{
		m_TrackSystem->Shutdown();
		delete m_TrackSystem;
		m_TrackSystem = 0;
	}

	//if(m_TestTrack)
	//{
	//	delete m_TestTrack;
	//	m_TestTrack = 0;
	//}

	return;
}


bool ApplicationClass::Frame()
{
	bool result;


	// Read the user input.
	result = m_Input->Frame();
	if(!result)
		return false;
	
	// Check if the user pressed escape and wants to exit the application.
	if(m_Input->IsEscapePressed() == true)
		return false;

	// Update the system stats.
	m_Timer->Frame();
	m_Fps->Frame();
	m_Cpu->Frame();

	// Update the FPS value in the text object.
	result = m_Text->SetFps(m_Fps->GetFps(), m_Direct3D->GetDeviceContext());
	if(!result)
		return false;
	
	// Update the CPU usage value in the text object.
	result = m_Text->SetCpu(m_Cpu->GetCpuPercentage(), m_Direct3D->GetDeviceContext());
	if(!result)
		return false;

	// Do the frame input processing.
	result = HandleInput(m_Timer->GetTime());
	if(!result)
		return false;

	result = UpdateTimers(m_Timer->GetTime());
	if(!result)
		return false;

	result = UpdateModelPositions(m_Timer->GetTime());
	if(!result)
		return false;

	// Render the graphics.
	result = RenderGraphics();
	if(!result)
		return false;

	return result;
}


bool ApplicationClass::HandleInput(float frameTime)
{
	bool keyDown, result;
	float posX, posY, posZ, rotX, rotY, rotZ;


	// Set the frame time for calculating the updated position.
	m_Position->SetFrameTime(frameTime);

	// Handle the input.
	keyDown = m_Input->IsQPressed();
	m_Position->TurnLeft(keyDown);

	keyDown = m_Input->IsEPressed();
	m_Position->TurnRight(keyDown);

	keyDown = m_Input->IsWPressed();
	m_Position->MoveForward(keyDown);

	keyDown = m_Input->IsSPressed();
	m_Position->MoveBackward(keyDown);

	keyDown = m_Input->IsVPressed();
	m_Position->MoveUpward(keyDown);

	keyDown = m_Input->IsCPressed();
	m_Position->MoveDownward(keyDown);

	keyDown = m_Input->IsPgUpPressed();
	m_Position->LookUpward(keyDown);

	keyDown = m_Input->IsPgDownPressed();
	m_Position->LookDownward(keyDown);

	keyDown = m_Input->IsAPressed();
	m_Position->StrafeLeft(keyDown);

	keyDown = m_Input->IsDPressed();
	m_Position->StrafeRight(keyDown);
	
	// Get the view point position/rotation.
	m_Position->GetPosition(posX, posY, posZ);
	m_Position->GetRotation(rotX, rotY, rotZ);

	// Set the position of the camera.
	m_Camera->SetPosition(posX, posY, posZ);
	m_Camera->SetRotation(rotX, rotY, rotZ);

	// Update the position values in the text object.
	result = m_Text->SetCameraPosition(posX, posY, posZ, m_Direct3D->GetDeviceContext());
	if(!result)
	{
		return false;
	}

	// Update the rotation values in the text object.
	result = m_Text->SetCameraRotation(rotX, rotY, rotZ, m_Direct3D->GetDeviceContext());
	if(!result)
	{
		return false;
	}

	return true;
}


bool ApplicationClass::UpdateTimers(float frameTime)
{
	bool result;

	m_TrainTestClass->SetFrameTime(frameTime);
	m_plantCollection->SetFrameTime(frameTime);

	return true;
}


bool ApplicationClass::UpdateModelPositions(float frameTime)
{
	bool result;

	//m_TrainTestClass->MoveToDestination();

	return true;
}


bool ApplicationClass::RenderGraphics()
{
	D3DXMATRIX worldMatrix, viewMatrix, projectionMatrix, orthoMatrix, modelWorldMatrix;
	bool result;


	// Clear the scene.
	m_Direct3D->BeginScene(0.0f, 0.0f, 0.0f, 1.0f);

	// Generate the view matrix based on the camera's position.
	m_Camera->Render();

	// Get the world, view, projection, and ortho matrices from the camera and Direct3D objects.
	m_Direct3D->GetWorldMatrix(worldMatrix);
	m_Camera->GetViewMatrix(viewMatrix);
	m_Direct3D->GetProjectionMatrix(projectionMatrix);
	m_Direct3D->GetOrthoMatrix(orthoMatrix);


	m_TerrainColor->Render(m_Direct3D->GetDeviceContext());
	result = m_ColorShader->Render(m_Direct3D->GetDeviceContext(), m_TerrainColor->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix, 
				       m_Light->GetDirection(), m_Light->GetAmbientColor(), m_Light->GetDiffuseColor());

	m_TrackSystem->Render(m_ColorShader, m_Direct3D, m_Light, worldMatrix, viewMatrix, projectionMatrix);
	m_plantCollection->Render(m_ColorShader, m_Direct3D, m_Light, viewMatrix, projectionMatrix);
	m_TrainTestClass->Render(m_ColorShader, m_Direct3D, m_Light, viewMatrix, projectionMatrix);

	m_Direct3D->TurnZBufferOff();
	m_Direct3D->TurnOnAlphaBlending();

	result = m_Text->Render(m_Direct3D->GetDeviceContext(), m_FontShader, worldMatrix, orthoMatrix);
	if(!result)
		return false;

	m_Direct3D->TurnOffAlphaBlending();
	m_Direct3D->TurnZBufferOn();

	// Present the rendered scene to the screen.
	m_Direct3D->EndScene();

	return true;
}
