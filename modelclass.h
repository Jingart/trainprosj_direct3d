////////////////////////////////////////////////////////////////////////////////
// Filename: modelclass.h
////////////////////////////////////////////////////////////////////////////////
#pragma once
#ifndef _MODELCLASS_H_
#define _MODELCLASS_H_


//////////////
// INCLUDES //
//////////////
#include <d3d11.h>
#include <d3dx10math.h>
#include <fstream>

using namespace std;

////////////////////////////////////////////////////////////////////////////////
// Class name: ModelClass
////////////////////////////////////////////////////////////////////////////////
class ModelClass
{
private:

	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR4 color;
		D3DXVECTOR3 normal;
	};

	struct ModelType
	{
		float x, y, z;
		float tu, tv;
		float nx, ny, nz;
	};

public:

	ModelClass();
	ModelClass(float x, float y, float z);
	ModelClass(const ModelClass&);
	~ModelClass();
	void Shutdown();
	bool Initialize(ID3D11Device*);
	bool Initialize(ID3D11Device*, char*);	
	void Render(ID3D11DeviceContext*);
	int GetIndexCount();
	void SetColor(D3DXVECTOR4 color);

protected:

	bool InitializeBuffers(ID3D11Device*);
	bool InitializeBuffers2(ID3D11Device*);
	bool LoadModel(char*);
	void RenderBuffers(ID3D11DeviceContext*);
	void ShutdownBuffers();
	void ReleaseModel();

protected:

	ID3D11Buffer *m_vertexBuffer; 
	ID3D11Buffer *m_indexBuffer;
	int m_vertexCount;
	int m_indexCount;
	ModelType* m_model;
	D3DXVECTOR4 m_color;
};

#endif