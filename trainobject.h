#pragma once

#include "modelobject.h"
#include "RailRoad.h"
#include <vector>
#include <list>


class TrainObject : public ModelObject
{
public:
	enum  TrainObjectEnum{ LOCOMOTIVE, CART_BOX, CART_COAL};
	TrainObject(void);
	//~trainobject(void);

	
	void SetTrack(RailRoad*);
	void SetCurrentPosition(list<RailRoad::TrackTile>::iterator);
	//void SetTrackDestination(int);

	list<RailRoad::TrackTile>::iterator* GetCurrentPosition();
	TrainObjectEnum GetTrainObjectType();

	void MoveForwardOnTrack();
	void MoveToDestination();

	void PushToNextTile();
	void PushToPreviousTile();

	void MoveForward();
	bool m_canMove;
protected:

	RailRoad* m_trackObject;
	list<RailRoad::TrackTile> m_track;
	list<RailRoad::TrackTile>::iterator m_currentPosition;
	//int m_destination;
	TrainObjectEnum m_trainObjectType;

	void UpdatePositionOnTrack();
	
};

