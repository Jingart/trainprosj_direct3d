#pragma once

#include <d3d11.h>
#include <d3dx10math.h>
#include <list>

using namespace std;

class TrackTileBase
{

protected:

	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR3 normal;
		D3DXVECTOR4 color;
	};

public:
	enum  TileType{RAILROAD, STATION};

	//TODO tiledirection
    struct TrackTile
	{
		int x;
	    int y;
		TileType type;
		//TrainStation* trainStation; 
	};

	TrackTileBase(void);
	~TrackTileBase(void);

	bool Initialize(ID3D11Device*);
	void Shutdown();
	void Render(ID3D11DeviceContext*);
	int GetIndexCount();
	void GetTrack(list<TrackTile>&);

	void PushOnStart(TrackTile);
	void PushOnEnd(TrackTile);


protected:

	bool InitializeBuffers(ID3D11Device*);
	void ShutdownBuffers();
	void RenderBuffers(ID3D11DeviceContext*);

	int m_vertexCount;
	int m_indexCount;
	ID3D11Buffer *m_vertexBuffer; 
	ID3D11Buffer *m_indexBuffer;
	D3DXVECTOR4 m_color; 
	list<TrackTile> trackList;
};

