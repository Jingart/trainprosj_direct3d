#include "tracktilebase.h"


TrackTileBase::TrackTileBase(void)
{

	m_vertexBuffer = 0;
	m_indexBuffer = 0;

	//m_color = D3DXVECTOR4(1.0f, 0.2f, 0.6f, 0.5f);

}


TrackTileBase::~TrackTileBase(void)
{
}


void TrackTileBase::Shutdown()
{

	// Release the vertex and index buffer.
	ShutdownBuffers();

	return;
}


void TrackTileBase::PushOnStart(TrackTile tile)
{
	trackList.push_front(tile);
}


void TrackTileBase::PushOnEnd(TrackTile tile)
{
	trackList.push_back(tile);
}


void TrackTileBase::Render(ID3D11DeviceContext* deviceContext)
{
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(deviceContext);

	return;
}


int TrackTileBase::GetIndexCount()
{
	return m_indexCount;
}


bool TrackTileBase::InitializeBuffers(ID3D11Device* device)
{
	VertexType* vertices;
	unsigned long* indices;
	int index;
	int indicesCount;
	float positionX, positionZ;
	D3D11_BUFFER_DESC vertexBufferDesc;
	D3D11_BUFFER_DESC indexBufferDesc;
    D3D11_SUBRESOURCE_DATA vertexData; 
	D3D11_SUBRESOURCE_DATA indexData;
	HRESULT result;


	m_vertexCount = 4 * trackList.size();
	m_indexCount = 6 * trackList.size();


	// Create the vertex array.
	vertices = new VertexType[m_vertexCount];
	if(!vertices)
	{
		return false;
	}

	// Create the index array.
	indices = new unsigned long[m_indexCount];
	if(!indices)
	{
		return false;
	}

	index = 0;
	indicesCount = 0;


	//for(int i=0; i<trackList.size(); i++)
	
	list<TrackTile>::iterator trackIterator;

	float trackX = 0;
	float trackY = 0;

    for(trackIterator = trackList.begin(); trackIterator != trackList.end(); trackIterator++)
	{

			trackX = (float)trackIterator->x;
			trackY = (float)trackIterator->y;

			//Lower left point
			positionX = trackX;
			positionZ = trackY;
			vertices[index].position = D3DXVECTOR3(positionX, 0.06f, positionZ);
			vertices[index].normal = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
			vertices[index].color = m_color;
			index++;

			//Upper left point
			positionX = trackX;
			positionZ = trackY + 1;
			vertices[index].position = D3DXVECTOR3(positionX, 0.06f, positionZ);
			vertices[index].normal = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
			vertices[index].color = m_color;
			index++;

			//Upper right point
			positionX = trackX + 1;
			positionZ = trackY + 1;
			vertices[index].position = D3DXVECTOR3(positionX, 0.06f, positionZ);
			vertices[index].normal = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
			vertices[index].color = m_color;
			index++;

			//Lower right point
			positionX = trackX + 1;
			positionZ = trackY;
			vertices[index].position = D3DXVECTOR3(positionX, 0.06f, positionZ);
			vertices[index].normal = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
			vertices[index].color = m_color;
			index++;


			indices[indicesCount] = index - 4;  
			indicesCount++;

			indices[indicesCount] = index - 3;  
			indicesCount++;

			indices[indicesCount] = index - 2;  
			indicesCount++;

			indices[indicesCount] = index - 2;  
			indicesCount++;

			indices[indicesCount] = index - 1;  
			indicesCount++;

			indices[indicesCount] = index - 4;  
			indicesCount++;

	}



	// Set up the description of the static vertex buffer.
    vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    vertexBufferDesc.ByteWidth = sizeof(VertexType) * m_vertexCount;
    vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    vertexBufferDesc.CPUAccessFlags = 0;
    vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
    vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
    result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if(FAILED(result))
		return false;

	// Set up the description of the static index buffer.
    indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_indexCount;
    indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    indexBufferDesc.CPUAccessFlags = 0;
    indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
    indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if(FAILED(result))
		return false;

	// Release the arrays now that the buffers have been created and loaded.
	delete [] vertices;
	vertices = 0;

	delete [] indices;
	indices = 0;

	return true;
}


void TrackTileBase::ShutdownBuffers()
{
	// Release the index buffer.
	if(m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = 0;
	}

	// Release the vertex buffer.
	if(m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = 0;
	}

	return;
}


void TrackTileBase::RenderBuffers(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride;
	unsigned int offset;


	// Set vertex buffer stride and offset.
	stride = sizeof(VertexType); 
	offset = 0;
    
	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

    // Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

    // Set the type of primitive that should be rendered from this vertex buffer, in this case a line list.
	//deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	
	return;


}


void TrackTileBase::GetTrack(list<TrackTile>& track)
{
	track = this->trackList;
}