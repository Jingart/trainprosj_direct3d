#pragma once
#include "modelobject.h"

class CargoClass : public ModelObject
{
public:
	enum  CargoEnum{BOX, COAL};

	CargoClass(void);
	CargoClass(D3DXVECTOR4 color);
	void SetCargoType(CargoEnum);
private:
	CargoEnum m_cargoType;
};

