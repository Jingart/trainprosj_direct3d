#include "train.h"


Train::Train(void)
{

}


Train::~Train(void)
{

}


bool Train::Initialize(ID3D11Device* device)
{
	bool result;

	D3DXVECTOR4 locomotiveColor = D3DXVECTOR4(0.2f, 0.2f, 0.2f, 1.0f);

	m_locomotive = new LocomotiveObject(locomotiveColor);
	m_trainobjects.push_back(new TrainCartClass()); 
	m_trainobjects.push_back(new TrainCartClass()); 
	m_trainobjects.push_back(new TrainCartClass()); 
	m_trainobjects.push_back(new TrainCartClass()); 

	m_locomotive->Initialize(device);

	for(int i = 0; i < m_trainobjects.size(); i++)
	{
		result = m_trainobjects[i]->Initialize(device);
		if(!result)
			return false;
	}


	CargoBox *cargo = new CargoBox();
	cargo->Initialize(device);
	m_trainobjects[2]->AddCargo(cargo);

	//CargoClass *cargo2 = new CargoClass();
	//cargo2->Initialize(device);
	//m_trainobjects[1]->AddCargo(cargo2);


	return true;
}


void Train::Shutdown()
{
	m_locomotive->Shutdown();

	for(int i = 0; i < m_trainobjects.size(); i++)
		m_trainobjects[i]->Shutdown();
}


void Train::SetTrack(RailRoad* trackObject)
{
	m_RailRoadTrack = trackObject;
	m_locomotive->SetTrack(m_RailRoadTrack);

	for(int i = 0; i < m_trainobjects.size(); i++)
		m_trainobjects[i]->SetTrack(m_RailRoadTrack);

	AlignTrainOnTrack();
	
}


void Train::AlignTrainOnTrack()
{

	int pushCounter = 0;
	for(int i = m_trainobjects.size(); i > 0; i--)
	{
		for(int j = 0; j < pushCounter; j++)
			m_trainobjects[i]->PushToNextTile();

		pushCounter++;
	}	

	for(int j = 0; j < pushCounter; j++)
			m_locomotive->PushToNextTile();

}


void Train::AlignLocomotive()
{

}


void Train::AlignTrainObjects()
{

}


void Train::MoveToDestination()
{
	m_locomotive->MoveToDestination();

	for(int i = 0; i < m_trainobjects.size(); i++)
		m_trainobjects[i]->MoveToDestination();
}


void Train::SetFrameTime(float time)
{
	m_locomotive->SetFrameTime(time);

	for(int i = 0; i < m_trainobjects.size(); i++)
		m_trainobjects[i]->SetFrameTime(time);

	return;
}


void Train::Render(ColorShaderClass* colorShader, D3DClass* direct3D, LightClass* light, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix)
{
	bool result;

	D3DXMATRIX modelWorldMatrix;

	m_locomotive->GetMatrix(modelWorldMatrix);
	m_locomotive->Render(direct3D->GetDeviceContext());
	result = colorShader->Render(direct3D->GetDeviceContext(), m_locomotive->GetIndexCount(), modelWorldMatrix, viewMatrix, projectionMatrix, 
					light->GetDirection(), light->GetAmbientColor(), light->GetDiffuseColor());

	for(int i = 0; i < m_trainobjects.size(); i++)
	{

		vector<CargoBox*> m_cargoList = m_trainobjects[i]->GetCargoVector();

		for (int i = 0; i < m_cargoList.size(); i++)
		{
			m_cargoList[i]->GetMatrix(modelWorldMatrix);
			m_cargoList[i]->Render(direct3D->GetDeviceContext());
			result = colorShader->Render(direct3D->GetDeviceContext(), m_cargoList[i]->GetIndexCount(), modelWorldMatrix, viewMatrix, projectionMatrix, 
							light->GetDirection(), light->GetAmbientColor(), light->GetDiffuseColor());
		}

		m_trainobjects[i]->GetMatrix(modelWorldMatrix);
		m_trainobjects[i]->Render(direct3D->GetDeviceContext());
		result = colorShader->Render(direct3D->GetDeviceContext(), m_trainobjects[i]->GetIndexCount(), modelWorldMatrix, viewMatrix, projectionMatrix, 
						   light->GetDirection(), light->GetAmbientColor(), light->GetDiffuseColor());
	}

}
