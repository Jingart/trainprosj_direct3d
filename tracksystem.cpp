#include "tracksystem.h"


TrackSystem::TrackSystem(void)
{
}


TrackSystem::~TrackSystem(void)
{
}

bool TrackSystem::Initialize(ID3D11Device* device)
{

	InitTestTracks(device);



	//m_RailroadTrackObjects.push_back(testTrack);

	InitTestStations(device);
	//m_TrainStationObjects.push_back(testStation);
	//m_TrainStationObjects.push_back(testStation2);

	list<TrackTileBase::TrackTile> tmp;
	testStation->GetTrack(tmp);

	//testTrack->PushOnStart(tmp.front());
	m_RailroadTrackObjects.push_back(testTrack);
	return true;
}


bool TrackSystem::InitTestTracks(ID3D11Device* device)
{

	//TODO placeholder
	testTrack = new RailRoad;
	if(!testTrack)
		return false;

	// Initialize the terrain object.
	bool result = testTrack->Initialize(device);
	if(!result)
	{
		return false;
	}

}


bool TrackSystem::InitTestStations(ID3D11Device* device)
{

	TrackTileBase::TrackTile start;
	TrackTileBase::TrackTile end;

	start.x = 10;
	start.y = 4;
	end.x = 10;
	end.y = 10;

	testStation = new TrainStation(start, end);
	if(!testStation)
		return false;

	start.x = 41;
	start.y = 1;
	end.x = 47;
	end.y = 1;

	testStation2 = new TrainStation(start, end);
	if(!testStation2)
		return false;


	// Initialize the terrain object.
	bool result = testStation->Initialize(device);
	if(!result)
	{
		return false;
	}


	// Initialize the terrain object.
	result = testStation2->Initialize(device);
	if(!result)
	{
		return false;
	}


	//TrainStation::TileCoordinat start;
	//TrainStation::TileCoordinat end;

	//start.x = 10;
	//start.y = 4;
	//end.x = 10;
	//end.y = 10;

	//m_trainStation = new TrainStation(start, end);
	//if(!m_trainStation)
	//	return false;

	//start.x = 41;
	//start.y = 1;
	//end.x = 47;
	//end.y = 1;

	//m_trainStation2 = new TrainStation(start, end);
	//if(!m_trainStation2)
	//	return false;


	//// Initialize the terrain object.
	//bool result = m_trainStation->Initialize(device);
	//if(!result)
	//{
	//	return false;
	//}


	//// Initialize the terrain object.
	//result = m_trainStation2->Initialize(device);
	//if(!result)
	//{
	//	return false;
	//}

}


void TrackSystem::Shutdown()
{
	//m_locomotive->Shutdown();

	for(int i = 0; i < m_RailroadTrackObjects.size(); i++)
		m_RailroadTrackObjects[i]->Shutdown();

	for(int i = 0; i < m_TrainStationObjects.size(); i++)
		m_TrainStationObjects[i]->Shutdown();
}


void TrackSystem::AddRailroadTrack(RailRoad* track)
{
	m_RailroadTrackObjects.push_back(track);
}


void TrackSystem::RemoveRailroadTrack(RailRoad* track)
{

}


void TrackSystem::AddTrainStation(TrainStation* station)
{
	m_TrainStationObjects.push_back(station);
}


void TrackSystem::RemoveTrainStation(TrainStation* station)
{

}


vector<TrainStation*> TrackSystem::getTrainStationCollection()
{
	return m_TrainStationObjects;
}


vector<RailRoad*> TrackSystem::getRailroadTrackCollection()
{
	return m_RailroadTrackObjects;
}


void TrackSystem::Render(ColorShaderClass* colorShader, D3DClass* direct3D, LightClass* light, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix)
{
	bool result;

	D3DXMATRIX modelWorldMatrix;

	for(int i = 0; i < m_RailroadTrackObjects.size(); i++)
	{
		m_RailroadTrackObjects[i]->Render(direct3D->GetDeviceContext());
		result = colorShader->Render(direct3D->GetDeviceContext(), m_RailroadTrackObjects[i]->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix, 
						   light->GetDirection(), light->GetAmbientColor(), light->GetDiffuseColor());
	}

	for(int i = 0; i < m_TrainStationObjects.size(); i++)
	{
		m_TrainStationObjects[i]->Render(direct3D->GetDeviceContext());
		result = colorShader->Render(direct3D->GetDeviceContext(), m_TrainStationObjects[i]->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix, 
						   light->GetDirection(), light->GetAmbientColor(), light->GetDiffuseColor());
	}
}