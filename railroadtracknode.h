#pragma once
#ifndef _RAILROADTRACKNODE_H_
#define _RAILROADTRACKNODE_H_


//////////////
// INCLUDES //
//////////////
#include <d3d11.h>
#include <d3dx10math.h>
//#include "double_linked.h"
#include <list>
#include "trainstation.h"


using namespace std;

class RailRoadTrackNode
{

private:

	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR3 normal;
		D3DXVECTOR4 color;
	};

public:
	enum  TileType{RAILROAD, STATION};

    struct RailRoadTile
	{
		int x;
	    int y;
		TileType type;
		TrainStation* trainStation; 
	};

	RailRoadTrackNode(void);
	RailRoadTrackNode(const RailRoadTrackNode&);
	~RailRoadTrackNode(void);

	bool Initialize(ID3D11Device*);
	void Shutdown();
	void Render(ID3D11DeviceContext*);
	int GetIndexCount();
    void GetTrack(list<RailRoadTile>&);

private:

	bool InitializeBuffers(ID3D11Device*);
	void ShutdownBuffers();
	void RenderBuffers(ID3D11DeviceContext*);

	int m_vertexCount;
	int m_indexCount;
	ID3D11Buffer *m_vertexBuffer; 
	ID3D11Buffer *m_indexBuffer;
	//vector<TileCoordinat> track;
	D3DXVECTOR4 m_color; 
	list<RailRoadTile> trackList;

};

#endif